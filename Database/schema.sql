USE [Multimascot]
GO
/****** Object:  StoredProcedure [dbo].[categoryCreate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- TABLE: Category
CREATE PROC [dbo].[categoryCreate]
	@Description	VARCHAR(100)
AS
	INSERT INTO Category (Description) VALUES (@Description)




GO
/****** Object:  StoredProcedure [dbo].[categoryFind]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[categoryFind]
	@ID				INT = 0
AS
	IF @ID = 0
		SELECT * FROM Category ORDER BY Description ASC
	ELSE
		SELECT * FROM Category WHERE ID = @ID ORDER BY Description ASC




GO
/****** Object:  StoredProcedure [dbo].[categoryUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[categoryUpdate]
	@ID				INT,
	@Description	VARCHAR(100)
AS
	UPDATE Category SET Description = @Description WHERE ID = @ID




GO
/****** Object:  StoredProcedure [dbo].[customerCreate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- TABLE: Customer
CREATE PROC [dbo].[customerCreate]
	@RUC			VARCHAR(50)		= NULL,
	@Name			VARCHAR(100)	= NULL,
	@Phone			VARCHAR(50)		= NULL,
	@Annotation		VARCHAR(300)	= NULL,
	@Email			VARCHAR(500)	= NULL,
	@Address		VARCHAR(500)	= NULL
AS
	INSERT INTO Customer 
	(RUC, Name,Phone, Annotation, Email, [Address]) VALUES 
	(@RUC, @Name, @Phone, @Annotation, @Email, @Address)

GO
/****** Object:  StoredProcedure [dbo].[customerFind]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[customerFind]
	@ID				INT				= 0,
	@Name			VARCHAR(100)	= NULL
AS
	IF @Name IS NOT NULL
		SELECT * FROM Customer c WHERE (c.RUC + c.Name) LIKE '%' + @Name + '%'
		ORDER BY c.Name ASC
	ELSE IF @ID = 0
		SELECT * FROM Customer ORDER BY Name ASC
	ELSE
		SELECT * FROM Customer WHERE ID = @ID




GO
/****** Object:  StoredProcedure [dbo].[customerUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[customerUpdate]
	@ID				INT,
	@RUC			VARCHAR(50)		= NULL,
	@Name			VARCHAR(100)	= NULL,
	@Phone			VARCHAR(50)		= NULL,
	@Annotation		VARCHAR(300)	= NULL,
	@Email			VARCHAR(500)	= NULL,
	@Address		VARCHAR(300)	= NULL
AS
	UPDATE Customer SET 
		RUC			= @RUC,
		Name		= @Name,
		Phone		= @Phone,
		Annotation	= @Annotation,
		Email		= @Email,
		[Address]	= @Address
	WHERE ID = @ID




GO
/****** Object:  StoredProcedure [dbo].[itemCreate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[itemCreate]
	@Code				VARCHAR(100)	= NULL,
	@Cost				DECIMAL(10,2)	= 0,
	@Description		VARCHAR(1000)	= NULL,
	@DescriptionShort	VARCHAR(1000)	= NULL,
	@ImageSmall			IMAGE			= NULL,
	@ImageBig			IMAGE			= NULL,
	@SupplierID			INT				= NULL,
	@CategoryID			INT				= NULL,
	@UnitPrice			DECIMAL(10,2)	= 0,
	@QuarterPrice		DECIMAL(10,2)	= 0,
	@DozenPrice			DECIMAL(10,2)	= 0,
	@QuantityPerUnit	VARCHAR(100)	= NULL,
	@UnitsInStock		INT				= 0,
	@Discontinued		BIT				= 0
AS
	
	INSERT INTO Item 
		(	
			Code, 
			Cost, 
			Description, 
			DescriptionShort,
			[ImageSmall],
			[ImageBig],
			SupplierID,
			CategoryID,
			UnitPrice,
			QuarterPrice,
			DozenPrice,
			QuantityPerUnit,
			UnitsInStock,
			Discontinued
		)	VALUES 
		(
			@Code, 
			@Cost, 
			@Description, 
			@DescriptionShort, 
			@ImageSmall,
			@ImageBig,
			@SupplierID,
			@CategoryID,
			@UnitPrice,
			@QuarterPrice,
			@DozenPrice,
			@QuantityPerUnit,
			@UnitsInStock,
			@Discontinued
		)




GO
/****** Object:  StoredProcedure [dbo].[itemFind]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[itemFind]
	@ID				INT				= 0,
	@Description	VARCHAR(200)	= NULL,
	@categoryID		INT				= 0
AS
	IF @categoryID > 0
	BEGIN
		IF @Description IS NULL
			SET @Description = ''

		SELECT	i.*,
				ISNULL(c.Description, '') AS CategoryDescription, 
				ISNULL(s.Description, '') AS SupplierDescription 
		FROM 
				Item i	LEFT JOIN Category c ON i.CategoryID = c.ID
						LEFT JOIN Supplier s ON i.SupplierID = s.ID
		WHERE (i.Description LIKE '%' + @Description + '%' OR i.Code LIKE '%' + @Description + '%')
		AND i.CategoryID = @categoryID
		ORDER BY 
				i.Description ASC
	END
	IF @Description IS NOT NULL
		SELECT	i.*,
				ISNULL(c.Description, '') AS CategoryDescription, 
				ISNULL(s.Description, '') AS SupplierDescription 
		FROM 
				Item i	LEFT JOIN Category c ON i.CategoryID = c.ID
						LEFT JOIN Supplier s ON i.SupplierID = s.ID
		WHERE i.Description LIKE '%' + @Description + '%' OR i.Code LIKE '%' + @Description + '%'
		ORDER BY 
				i.Description ASC
	ELSE IF @ID = 0
		SELECT	i.*,
				ISNULL(c.Description, '') AS CategoryDescription, 
				ISNULL(s.Description, '') AS SupplierDescription 
		FROM 
				Item i	LEFT JOIN Category c ON i.CategoryID = c.ID
						LEFT JOIN Supplier s ON i.SupplierID = s.ID
		ORDER BY 
				i.Description ASC
	ELSE
		SELECT	i.*,
				ISNULL(c.Description, '') AS CategoryDescription, 
				ISNULL(s.Description, '') AS SupplierDescription 
		FROM 
				Item i	LEFT JOIN Category c ON i.CategoryID = c.ID
						LEFT JOIN Supplier s ON i.SupplierID = s.ID
		WHERE i.ID = @ID




GO
/****** Object:  StoredProcedure [dbo].[itemUnitsInStock]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[itemUnitsInStock]
	@unitsInStock INT
AS
SELECT 
	i.ID, 
	i.Code, 
	i.Description, 
	i.ImageSmall, 
	i.UnitsInStock, 
	c.Description AS CategoryDescription
FROM 
	Item i LEFT JOIN Category c ON i.CategoryID = c.ID
WHERE i.UnitsInStock <= @unitsInStock
AND i.Discontinued = 0




GO
/****** Object:  StoredProcedure [dbo].[itemUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[itemUpdate]
	@ID					INT,
	@Code				VARCHAR(100)	= NULL,
	@Cost				DECIMAL(10,2)	= 0,
	@Description		VARCHAR(1000)	= NULL,
	@DescriptionShort	VARCHAR(1000)	= NULL,
	@ImageSmall			IMAGE			= NULL,
	@ImageBig			IMAGE			= NULL,
	@SupplierID			INT				= NULL,
	@CategoryID			INT				= NULL,
	@UnitPrice			DECIMAL(10,2)	= 0,
	@QuarterPrice		DECIMAL(10,2)	= 0,
	@DozenPrice			DECIMAL(10,2)	= 0,
	@QuantityPerUnit	VARCHAR(100)	= NULL,
	@UnitsInStock		INT				= 0,
	@Discontinued		BIT				= 0
AS
	UPDATE Item SET 
		Code			= @Code,
		Cost			= @Cost,
		Description		= @Description,
		DescriptionShort = @DescriptionShort,
		ImageSmall		= @ImageSmall,
		ImageBig		= @ImageBig,
		SupplierID		= @SupplierID,
		CategoryID		= @CategoryID,
		UnitPrice		= @UnitPrice,
		QuarterPrice	= @QuarterPrice,
		DozenPrice		= @DozenPrice,
		QuantityPerUnit = @QuantityPerUnit,
		UnitsInStock	= @UnitsInStock,
		Discontinued	= @Discontinued
	WHERE ID = @ID




GO
/****** Object:  StoredProcedure [dbo].[saleCreate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- TABLE: Sale
CREATE PROC [dbo].[saleCreate]
	@CustomerID			INT				= NULL,
	@UserID				INT				= NULL,
	@TaxID				INT				= NULL,
	@Date				DATE			= NULL,
	@Status				VARCHAR(50)		= 'activo',
	@Annotation			VARCHAR(300)	= NULL,
	@ID					INT OUTPUT
AS
	SET @Date = GETDATE()

	IF @TaxID = 0
		SET @TaxID = NULL

	INSERT INTO Sale 
		(
			CustomerID,		UserID,
			TaxID,			Date,
			Status,			Annotation
		) VALUES 
		(
			@CustomerID,	@UserID,
			@TaxID,			@Date,
			@Status,		@Annotation
		)
	SET @ID = @@IDENTITY




GO
/****** Object:  StoredProcedure [dbo].[saleDetailCreate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- TABLE: SaleDetail
CREATE PROC [dbo].[saleDetailCreate]
	@SaleID				INT				= 0,
	@ItemID				INT				= 0,
	@RealUnitPrice		DECIMAL(10,2)	= 0,
	@Quantity			INT				= 0,
	@Discount			DECIMAL(10,2)	= 0,
	@Annotation			VARCHAR(300)	= NULL
AS
	
	DECLARE 
	@Cost					DECIMAL(10,2),	@UnitPriceOnSale	DECIMAL(10,2),
	@QuarterPriceOnSale		DECIMAL(10,2),	@DozenPriceOnSale	DECIMAL(10,2),
	@QuantityPerUnitOnSale	VARCHAR(100),	@UnitsInStock		INT,
	@Description			VARCHAR(200)

	SELECT 
		@Cost					= Cost,
		@UnitPriceOnSale		= UnitPrice,
		@QuarterPriceOnSale		= QuarterPrice,
		@DozenPriceOnSale		= DozenPrice,
		@QuantityPerUnitOnSale	= QuantityPerUnit,
		@UnitsInStock			= UnitsInStock,
		@Description			= Description
	FROM Item 
	WHERE ID = @ItemID

	IF ( @Quantity > @UnitsInStock )
	BEGIN
		DECLARE @MSG VARCHAR(500)
		SET		@MSG = 'La cantidad('+CAST(@Quantity AS VARCHAR)+') supera el stock('+CAST(@UnitsInStock AS VARCHAR)+') del producto "' + @Description + '"' + CHAR(13) + CHAR(13) + '(Es probable que el producto haya sido vendido durante el proceso de esta venta)'
		RAISERROR (@MSG,16,1)
		RETURN
	END

	INSERT INTO SaleDetail
		(
			SaleID,					ItemID,
			Cost,					UnitPriceOnSale,
			QuarterPriceOnSale,		DozenPriceOnSale,
			QuantityPerUnitOnSale,	RealUnitPrice,
			Quantity,				Discount,
			Annotation
		)
		VALUES
		(
			@SaleID,					@ItemID,
			@Cost,						@UnitPriceOnSale,
			@QuarterPriceOnSale,		@DozenPriceOnSale,
			@QuantityPerUnitOnSale,		@RealUnitPrice,
			@Quantity,					@Discount,
			@Annotation
		)

	UPDATE Item SET UnitsInStock = (UnitsInStock - @Quantity) WHERE ID = @ItemID




GO
/****** Object:  StoredProcedure [dbo].[saleDetailFind]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[saleDetailFind]
	@SaleID	INT = 0
AS
	SELECT 
		s.ID,	
		i.Code											AS Code,
		i.QuantityPerUnit								AS Talla,
		i.Description									AS Nombre,
		i.DescriptionShort								AS Descripcion,
		sd.Cost											AS Costo,
		sd.UnitPriceOnSale								AS [Precio unitario en Venta],
		sd.QuarterPriceOnSale							AS [Precio x /4 en Venta],
		sd.DozenPriceOnSale								AS [Precio x /12 en Venta],
		sd.QuantityPerUnitOnSale						AS [Talla],
		sd.RealUnitPrice								AS [Precio de venta],
		sd.Quantity										AS [Cantidad],
		(sd.RealUnitPrice * sd.Quantity)				AS [Pre. x Cant.],
		ISNULL((SELECT TOP 1 Percentage FROM Tax ORDER BY CreatedAt DESC),0)							AS [% IGV],	
			
		(sd.Discount)									AS Descuento,

		CONVERT(DECIMAL(10,2),((sd.RealUnitPrice * sd.Quantity) - (sd.Discount)) - 
		((sd.RealUnitPrice * sd.Quantity) - (sd.Discount)) * ISNULL((SELECT TOP 1 Percentage FROM Tax ORDER BY CreatedAt DESC),0)) AS SubTotal,

		CONVERT(DECIMAL(10,2),((sd.RealUnitPrice * sd.Quantity) - (sd.Discount)) * ISNULL((SELECT TOP 1 Percentage FROM Tax ORDER BY CreatedAt DESC),0)) AS IGV,

		CONVERT(DECIMAL(10,2),ROUND((sd.RealUnitPrice * sd.Quantity) - (sd.Discount),2))  AS Total,

		sd.Annotation										AS [Observacion]

		FROM 
			Sale s	INNER JOIN	Customer c		ON s.CustomerID = c.ID 
					INNER JOIN	[User] u		ON s.UserID = u.ID
					INNER JOIN	SaleDetail sd	ON s.ID = sd.SaleID
					INNER JOIN	Item i			ON sd.ItemID = i.ID
		WHERE s.ID = @SaleID
		ORDER BY s.CreatedAt DESC




GO
/****** Object:  StoredProcedure [dbo].[saleFind]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[saleFind] --1
	@ID				INT = 0,
	@DATE			VARCHAR(10) = NULL
AS
	IF @ID = 0
		SELECT 
			s.ID,		
			c.Name,
			u.Account,	
			ISNULL((SELECT TOP 1 Percentage FROM Tax ORDER BY CreatedAt DESC),0) AS Percentage,
			s.Date,		
			s.Status,
			CONVERT(DECIMAL(10,2),SUM(sd.RealUnitPrice * sd.Quantity))				AS [PrexCant],
			
			SUM(sd.Discount)								AS Discount,


			CONVERT(DECIMAL(10,2),(SUM(sd.RealUnitPrice * sd.Quantity) - SUM(sd.Discount)) - 
			(SUM(sd.RealUnitPrice * sd.Quantity) - SUM(sd.Discount)) * ISNULL((SELECT TOP 1 Percentage FROM Tax ORDER BY CreatedAt DESC),0)) AS SubTotal,

			CONVERT(DECIMAL(10,2),(SUM(sd.RealUnitPrice * sd.Quantity) - SUM(sd.Discount)) * ISNULL((SELECT TOP 1 Percentage FROM Tax ORDER BY CreatedAt DESC),0)) AS IGV,

			CONVERT(DECIMAL(10,2),ROUND(SUM(sd.RealUnitPrice * sd.Quantity) - SUM(sd.Discount),2),0)  AS Total,

			s.CreatedAt,
			s.UpdatedAt,
			s.Annotation

			FROM 
				Sale s	INNER JOIN	Customer c		ON s.CustomerID = c.ID 
						INNER JOIN	[User] u		ON s.UserID = u.ID
						INNER JOIN	SaleDetail sd	ON s.ID = sd.SaleID
			WHERE
				CONVERT(VARCHAR(10), s.Date, 103) = @DATE
			GROUP BY
				s.ID,		
				c.Name,
				u.Account,	
				s.Date,		
				s.Status,
				s.CreatedAt,
				s.UpdatedAt,
				s.Annotation
			ORDER BY s.CreatedAt DESC
	ELSE
		SELECT 
			s.ID,		
			c.Name,
			c.RUC,
			u.Account,	
			ISNULL((SELECT TOP 1 Percentage FROM Tax ORDER BY CreatedAt DESC),0) AS Percentage,
			s.Date,		
			s.Status,
			CONVERT(DECIMAL(10,2),SUM(sd.RealUnitPrice * sd.Quantity))				AS [PrexCant],
			
			SUM(sd.Discount)								AS Discount,


			CONVERT(DECIMAL(10,2),(SUM(sd.RealUnitPrice * sd.Quantity) - SUM(sd.Discount)) - 
			(SUM(sd.RealUnitPrice * sd.Quantity) - SUM(sd.Discount)) * ISNULL((SELECT TOP 1 Percentage FROM Tax ORDER BY CreatedAt DESC),0)) AS SubTotal,

			CONVERT(DECIMAL(10,2),(SUM(sd.RealUnitPrice * sd.Quantity) - SUM(sd.Discount)) * ISNULL((SELECT TOP 1 Percentage FROM Tax ORDER BY CreatedAt DESC),0)) AS IGV,

			CONVERT(DECIMAL(10,2),ROUND(SUM(sd.RealUnitPrice * sd.Quantity) - SUM(sd.Discount),2))  AS Total,

			s.CreatedAt,
			s.UpdatedAt,
			s.Annotation

			FROM 
				Sale s	INNER JOIN	Customer c		ON s.CustomerID = c.ID 
						INNER JOIN	[User] u		ON s.UserID = u.ID
						INNER JOIN	SaleDetail sd	ON s.ID = sd.SaleID
			WHERE s.ID = @ID
			GROUP BY
				s.ID,		
				c.Name,
				c.RUC,
				u.Account,	
				s.Date,		
				s.Status,
				s.CreatedAt,
				s.UpdatedAt,
				s.Annotation




GO
/****** Object:  StoredProcedure [dbo].[saleUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[saleUpdate]
	@ID				INT,
	@Status			VARCHAR(300)	= 'activo',
	@Annotation		VARCHAR(300)
AS
	UPDATE Sale 
		SET
			Status = @Status,
			Annotation = @Annotation
		WHERE ID = @ID




GO
/****** Object:  StoredProcedure [dbo].[supplierCreate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[supplierCreate]
	@Description	VARCHAR(100)	= NULL,
	@RUC			VARCHAR(50)		= NULL,
	@Phone			VARCHAR(50)		= NULL,
	@Annotation		VARCHAR(300)	= NULL,
	@Address		VARCHAR(300)	= NULL
AS
	INSERT INTO Supplier 
	(Description, RUC, Phone, Annotation, [Address]) VALUES 
	(@Description, @RUC, @Phone, @Annotation, @Address)




GO
/****** Object:  StoredProcedure [dbo].[supplierFind]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[supplierFind]
	@ID				INT = 0
AS
	IF @ID = 0
		SELECT * FROM Supplier ORDER BY Description ASC
	ELSE
		SELECT * FROM Supplier WHERE ID = @ID ORDER BY Description ASC




GO
/****** Object:  StoredProcedure [dbo].[supplierUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[supplierUpdate]
	@ID				INT,
	@Description	VARCHAR(100)	= NULL,
	@RUC			VARCHAR(50)		= NULL,
	@Phone			VARCHAR(50)		= NULL,
	@Annotation		VARCHAR(300)	= NULL,
	@Address		VARCHAR(300)	= NULL
AS
	UPDATE Supplier SET 
		Description = @Description,
		RUC			= @RUC,
		Phone		= @Phone,
		Annotation	= @Annotation,
		[Address]	= @Address
	WHERE ID = @ID




GO
/****** Object:  StoredProcedure [dbo].[taxCreate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***************************************************/
/********************* SP **************************/
/***************************************************/

-- TABLE: Tax
CREATE PROC [dbo].[taxCreate]
	@Percentage		DECIMAL(10,2)
AS
	INSERT INTO Tax (Percentage) VALUES (@Percentage)




GO
/****** Object:  StoredProcedure [dbo].[taxFind]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[taxFind]
	@ID				INT = 0
AS
	IF @ID = 0
		SELECT * FROM Tax ORDER BY CreatedAt DESC
	ELSE
		SELECT * FROM Tax WHERE ID = @ID




GO
/****** Object:  StoredProcedure [dbo].[userChangePassword]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[userChangePassword]
	@ID				INT		= NULL,
	@Password		VARCHAR(200)	= NULL
AS
	UPDATE [User] SET Password = @Password
	WHERE ID = @ID




GO
/****** Object:  StoredProcedure [dbo].[userCreate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- TABLE: User
CREATE PROC [dbo].[userCreate]
	@Name			VARCHAR(100)	= NULL,
	@IdentityCard	VARCHAR(50)		= NULL,
	@Phone			VARCHAR(50)		= NULL,
	@Account		VARCHAR(20)		= NULL,
	@Password		VARCHAR(200)	= NULL,
	@Status			BIT				= 1,
	@Admin			BIT				= 0
AS
	INSERT INTO [User] 
		(
			Name,			IdentityCard,
			Phone,			Account,
			[Password],		[Status],
			[Admin]
		) VALUES 
		(
			@Name,			@IdentityCard,
			@Phone,			@Account,
			@Password,		@Status,
			@Admin
		)




GO
/****** Object:  StoredProcedure [dbo].[userFind]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[userFind]
	@ID				INT = 0
AS
	IF @ID = 0
		SELECT * FROM [User] ORDER BY Name ASC
	ELSE
		SELECT * FROM [User] WHERE ID = @ID




GO
/****** Object:  StoredProcedure [dbo].[userLogin]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[userLogin]
	@Account		VARCHAR(20)		= NULL,
	@Password		VARCHAR(200)	= NULL
AS
	SELECT 
		TOP 1
		* 
	FROM [User] u
	WHERE u.Account = @Account AND u.Password = @Password




GO
/****** Object:  StoredProcedure [dbo].[userUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[userUpdate]
	@ID				INT				= 0,
	@Name			VARCHAR(100)	= NULL,
	@IdentityCard	VARCHAR(50)		= NULL,
	@Phone			VARCHAR(50)		= NULL,
	@Account		VARCHAR(20)		= NULL,
	@Password		VARCHAR(200)	= NULL,
	@Status			BIT				= 1,
	@Admin			BIT				= 0
AS
	UPDATE [User] SET 
		Name			= @Name,
		IdentityCard	= @IdentityCard,
		Phone			= @Phone,
		Account			= @Account,
		[Status]		= @Status,
		[Admin]			= @Admin
 	WHERE ID = @ID

	IF @Password IS NOT NULL
		UPDATE [User] SET [Password] = @Password  WHERE ID = @ID




GO
/****** Object:  Table [dbo].[Category]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NULL,
	[UpdatedAt] [datetime] NULL,
	[CreatedAt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RUC] [varchar](50) NULL,
	[Name] [varchar](100) NULL,
	[Phone] [varchar](50) NULL,
	[Annotation] [varchar](300) NULL,
	[UpdatedAt] [datetime] NULL,
	[CreatedAt] [datetime] NULL,
	[Email] [varchar](500) NULL,
	[Address] [varchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Item]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Item](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](100) NULL,
	[Cost] [decimal](10, 2) NULL,
	[Description] [varchar](1000) NULL,
	[ImageSmall] [image] NULL,
	[ImageBig] [image] NULL,
	[SupplierID] [int] NULL,
	[CategoryID] [int] NULL,
	[UnitPrice] [decimal](10, 2) NULL,
	[QuarterPrice] [decimal](10, 2) NULL,
	[DozenPrice] [decimal](10, 2) NULL,
	[QuantityPerUnit] [varchar](100) NULL,
	[UnitsInStock] [int] NULL,
	[Discontinued] [bit] NULL,
	[UpdatedAt] [datetime] NULL,
	[CreatedAt] [datetime] NULL,
	[DescriptionShort] [varchar](500) NULL,
 CONSTRAINT [PK__Item__3214EC27E5AB72DE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sale]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sale](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NULL,
	[UserID] [int] NULL,
	[TaxID] [int] NULL,
	[Date] [date] NULL,
	[Status] [varchar](50) NULL,
	[Annotation] [varchar](300) NULL,
	[UpdatedAt] [datetime] NULL,
	[CreatedAt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SaleDetail]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SaleDetail](
	[SaleID] [int] NULL,
	[ItemID] [int] NULL,
	[Cost] [decimal](10, 2) NULL,
	[UnitPriceOnSale] [decimal](10, 2) NULL,
	[QuarterPriceOnSale] [decimal](10, 2) NULL,
	[DozenPriceOnSale] [decimal](10, 2) NULL,
	[QuantityPerUnitOnSale] [varchar](100) NULL,
	[RealUnitPrice] [decimal](10, 2) NULL,
	[Quantity] [int] NULL,
	[Discount] [decimal](10, 2) NULL,
	[Annotation] [varchar](300) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Supplier](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](200) NULL,
	[RUC] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Annotation] [varchar](300) NULL,
	[UpdatedAt] [datetime] NULL,
	[CreatedAt] [datetime] NULL,
	[Address] [varchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tax]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tax](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Percentage] [decimal](10, 2) NULL,
	[UpdatedAt] [datetime] NULL,
	[CreatedAt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[IdentityCard] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Account] [varchar](20) NULL,
	[Password] [varchar](200) NULL,
	[Status] [bit] NULL,
	[Admin] [bit] NULL,
	[UpdatedAt] [datetime] NULL,
	[CreatedAt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Account] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_ITEM_R1] FOREIGN KEY([SupplierID])
REFERENCES [dbo].[Supplier] ([ID])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_ITEM_R1]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_ITEM_R2] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_ITEM_R2]
GO
ALTER TABLE [dbo].[Sale]  WITH CHECK ADD  CONSTRAINT [FK_SALE_R1] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Sale] CHECK CONSTRAINT [FK_SALE_R1]
GO
ALTER TABLE [dbo].[Sale]  WITH CHECK ADD  CONSTRAINT [FK_SALE_R2] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Sale] CHECK CONSTRAINT [FK_SALE_R2]
GO
ALTER TABLE [dbo].[Sale]  WITH CHECK ADD  CONSTRAINT [FK_SALE_R3] FOREIGN KEY([TaxID])
REFERENCES [dbo].[Tax] ([ID])
GO
ALTER TABLE [dbo].[Sale] CHECK CONSTRAINT [FK_SALE_R3]
GO
ALTER TABLE [dbo].[SaleDetail]  WITH CHECK ADD  CONSTRAINT [FK_SALEDETAIL_R1] FOREIGN KEY([SaleID])
REFERENCES [dbo].[Sale] ([ID])
GO
ALTER TABLE [dbo].[SaleDetail] CHECK CONSTRAINT [FK_SALEDETAIL_R1]
GO
ALTER TABLE [dbo].[SaleDetail]  WITH CHECK ADD  CONSTRAINT [FK_SALEDETAIL_R2] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[SaleDetail] CHECK CONSTRAINT [FK_SALEDETAIL_R2]
GO
/****** Object:  Trigger [dbo].[categoryAfterInsert]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[categoryAfterInsert] ON [dbo].[Category] 
FOR INSERT
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE Category SET CreatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[categoryAfterUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[categoryAfterUpdate] ON [dbo].[Category] 
FOR UPDATE
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE Category SET UpdatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[customerAfterInsert]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[customerAfterInsert] ON [dbo].[Customer] 
FOR INSERT
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE Customer SET CreatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[customerAfterUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[customerAfterUpdate] ON [dbo].[Customer]  
FOR UPDATE
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE Customer SET UpdatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[itemAfterInsert]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[itemAfterInsert] ON [dbo].[Item] 
FOR INSERT
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE Item SET CreatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[itemAfterUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[itemAfterUpdate] ON [dbo].[Item]  
FOR UPDATE
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE Item SET UpdatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[saleAfterInsert]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[saleAfterInsert] ON [dbo].[Sale] 
FOR INSERT
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE Sale SET CreatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[saleAfterUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[saleAfterUpdate] ON [dbo].[Sale]  
FOR UPDATE
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE Sale SET UpdatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[supplierAfterInsert]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[supplierAfterInsert] ON [dbo].[Supplier] 
FOR INSERT
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE Supplier SET CreatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[supplierAfterUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[supplierAfterUpdate] ON [dbo].[Supplier]  
FOR UPDATE
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE Supplier SET UpdatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[taxAfterInsert]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[taxAfterInsert] ON [dbo].[Tax] 
FOR INSERT
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE [Tax] SET CreatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[taxAfterUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[taxAfterUpdate] ON [dbo].[Tax]  
FOR UPDATE
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE [Tax] SET UpdatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[userAfterInsert]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[userAfterInsert] ON [dbo].[User] 
FOR INSERT
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE [User] SET CreatedAt = GETDATE() WHERE ID = @ID




GO
/****** Object:  Trigger [dbo].[userAfterUpdate]    Script Date: 19/03/2015 09:47:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[userAfterUpdate] ON [dbo].[User]  
FOR UPDATE
AS
	DECLARE @ID INT;
    SELECT @ID =i.ID from inserted i;
    UPDATE [User] SET UpdatedAt = GETDATE() WHERE ID = @ID




GO
