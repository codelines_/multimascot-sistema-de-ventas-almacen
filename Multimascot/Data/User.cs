﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace Data
{
    public class User
    {
        public int create(ref string msgError, Entity.User user)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("userCreate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Name",            user.Name);
                command.Parameters.AddWithValue("@IdentityCard",    user.IdentityCard);
                command.Parameters.AddWithValue("@Phone",           user.Phone);
                command.Parameters.AddWithValue("@Account",         user.Account);
                command.Parameters.AddWithValue("@Password",        user.Password);
                command.Parameters.AddWithValue("@Status",          user.Status);
                command.Parameters.AddWithValue("@Admin",           user.Admin);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public int update(ref string msgError, Entity.User user)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("userUpdate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID",              user.ID);
                command.Parameters.AddWithValue("@Name",            user.Name);
                command.Parameters.AddWithValue("@IdentityCard",    user.IdentityCard);
                command.Parameters.AddWithValue("@Phone",           user.Phone);
                command.Parameters.AddWithValue("@Account",         user.Account);
                command.Parameters.AddWithValue("@Password",        user.Password);
                command.Parameters.AddWithValue("@Status",          user.Status);
                command.Parameters.AddWithValue("@Admin",           user.Admin);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public List<Entity.User> find(ref string msgError, int ID = 0)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            List<Entity.User> list = new List<Entity.User>();
            try
            {
                SqlCommand command = new SqlCommand("userFind", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID", ID);
                connection.Open();
                SqlDataReader dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Entity.User item = new Entity.User();
                        item.ID = dr["ID"] as int? ?? 0;
                        item.Name = dr["Name"] as string;
                        item.IdentityCard = dr["IdentityCard"] as string;
                        item.Phone = dr["Phone"] as string;
                        item.Account = dr["Account"] as string;
                        item.Password = dr["Password"] as string;
                        item.Status = dr["Status"] as bool? ?? false;
                        item.Admin = dr["Admin"] as bool? ?? false;
                        item.UpdatedAt = Convert.ToDateTime(dr["UpdatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        item.CreatedAt = Convert.ToDateTime(dr["CreatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        list.Add(item);
                    }
                }
                connection.Close();
                return list;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return list;
            }
        }

        public List<Entity.User> login(ref string msgError, string Account, string Password)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            List<Entity.User> list = new List<Entity.User>();
            try
            {
                SqlCommand command = new SqlCommand("userLogin", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Account",  Account);
                command.Parameters.AddWithValue("@Password", Password);
                connection.Open();
                SqlDataReader dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Entity.User item = new Entity.User();
                        item.ID             = dr["ID"] as int? ?? 0;
                        item.Name           = dr["Name"] as string;
                        item.IdentityCard   = dr["IdentityCard"] as string;
                        item.Phone          = dr["Phone"] as string;
                        item.Account        = dr["Account"] as string;
                        item.Password       = dr["Password"] as string;
                        item.Status         = dr["Status"] as bool? ?? false;
                        item.Admin          = dr["Admin"] as bool? ?? false;
                        item.UpdatedAt      = Convert.ToDateTime(dr["UpdatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        item.CreatedAt      = Convert.ToDateTime(dr["CreatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        list.Add(item);
                    }
                }
                connection.Close();
                return list;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return list;
            }
        }

        public int changePassword(ref string msgError, Entity.User user)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("userChangePassword", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID", user.ID);
                command.Parameters.AddWithValue("@Password", user.Password);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

    }
}
