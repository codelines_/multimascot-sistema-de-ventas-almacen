﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace Data
{
    public class Tax
    {
        public int create(ref string msgError, Entity.Tax tax)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("taxCreate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Percentage", tax.Percentage);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public List<Entity.Tax> find(ref string msgError)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            List<Entity.Tax> list = new List<Entity.Tax>();
            try
            {
                SqlCommand command = new SqlCommand("taxFind", connection);
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();
                SqlDataReader dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Entity.Tax item = new Entity.Tax();
                        item.ID             = dr["ID"] as int? ?? 0;
                        item.Percentage     = Convert.ToDouble(dr["Percentage"]);
                        item.UpdatedAt      = Convert.ToDateTime(dr["UpdatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        item.CreatedAt      = Convert.ToDateTime(dr["CreatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        list.Add(item);
                    }
                }
                connection.Close();
                return list;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return list;
            }
        }
    }
}
