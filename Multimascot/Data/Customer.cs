﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace Data
{
    public class Customer
    {
        public int create(ref string msgError, Entity.Customer customer)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("customerCreate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Name",        customer.Name);
                command.Parameters.AddWithValue("@RUC",         customer.RUC);
                command.Parameters.AddWithValue("@Phone",       customer.Phone);
                command.Parameters.AddWithValue("@Annotation",  customer.Annotation);
                command.Parameters.AddWithValue("@Email",       customer.Email);
                command.Parameters.AddWithValue("@Address",     customer.Address);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public int update(ref string msgError, Entity.Customer customer)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("customerUpdate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID",          customer.ID);
                command.Parameters.AddWithValue("@Name",        customer.Name);
                command.Parameters.AddWithValue("@RUC",         customer.RUC);
                command.Parameters.AddWithValue("@Phone",       customer.Phone);
                command.Parameters.AddWithValue("@Annotation",  customer.Annotation);
                command.Parameters.AddWithValue("@Email",       customer.Email);
                command.Parameters.AddWithValue("@Address",     customer.Address);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public List<Entity.Customer> find(ref string msgError, int ID = 0, string Name = null)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            List<Entity.Customer> list = new List<Entity.Customer>();
            try
            {
                SqlCommand command = new SqlCommand("customerFind", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID", ID);
                command.Parameters.AddWithValue("@Name", Name);
                connection.Open();
                SqlDataReader dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Entity.Customer item = new Entity.Customer();
                        item.ID             = dr["ID"] as int? ?? 0;
                        item.Name           = dr["Name"] as string;
                        item.RUC            = dr["RUC"] as string;
                        item.Phone          = dr["Phone"] as string;
                        item.Annotation     = dr["Annotation"] as string;
                        item.Email          = dr["Email"] as string;
                        item.Address        = dr["Address"] as string;
                        item.UpdatedAt      = Convert.ToDateTime(dr["UpdatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        item.CreatedAt      = Convert.ToDateTime(dr["CreatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        list.Add(item);
                    }
                }
                connection.Close();
                return list;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return list;
            }
        }

    }
}
