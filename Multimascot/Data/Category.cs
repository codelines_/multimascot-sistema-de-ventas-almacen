﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace Data
{
    public class Category
    {
        public int create(ref string msgError, Entity.Category category)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("categoryCreate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Description", category.Description);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public int update(ref string msgError, Entity.Category category)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("categoryUpdate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID",          category.ID);
                command.Parameters.AddWithValue("@Description", category.Description);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public List<Entity.Category> find(ref string msgError, int ID = 0)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            List<Entity.Category> list = new List<Entity.Category>();
            try
            {
                SqlCommand command = new SqlCommand("categoryFind", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID", ID);
                connection.Open();
                SqlDataReader dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Entity.Category item = new Entity.Category();
                        item.ID             = dr["ID"] as int? ?? 0;
                        item.Description    = dr["Description"] as string;
                        item.UpdatedAt      = Convert.ToDateTime(dr["UpdatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        item.CreatedAt      = Convert.ToDateTime(dr["CreatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        list.Add(item);
                    }
                }
                connection.Close();
                return list;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return list;
            }
        }

    }
}
