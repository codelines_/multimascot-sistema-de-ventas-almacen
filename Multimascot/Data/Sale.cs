﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace Data
{
    public class Sale
    {
        public int create(ref string msgError, Entity.Sale sale, List<Entity.Item> items)
        {
            using (var connection = new SqlConnection(ConnectionStr.Multimascot))
            {
                SqlTransaction trans = null;
                try
                {
                    connection.Open();
                    trans = connection.BeginTransaction();

                    using (SqlCommand command = new SqlCommand("saleCreate", connection, trans))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@CustomerID",      sale.CustomerID);
                        command.Parameters.AddWithValue("@UserID",          sale.UserID);
                        command.Parameters.AddWithValue("@TaxID",           sale.taxID);
                        command.Parameters.AddWithValue("@Status",          sale.Status);
                        command.Parameters.AddWithValue("@Annotation",      sale.Annotation);
                        var _ID = command.Parameters.AddWithValue("@ID",    sale.ID);
                        _ID.Direction = ParameterDirection.InputOutput;

                        int resp = command.ExecuteNonQuery();

                        if (resp > 0)
                        {
                            int saleID = Convert.ToInt32(_ID.Value);
                            command.CommandText = "saleDetailCreate";

                            foreach (var item in items)
                            {
                                command.Parameters.Clear();
                                command.Parameters.AddWithValue("@SaleID",          saleID);
                                command.Parameters.AddWithValue("@ItemID",          item.ID);
                                command.Parameters.AddWithValue("@RealUnitPrice",   item.SuggestedPriceOnSale);
                                command.Parameters.AddWithValue("@Quantity",        item.QuantityOnSale);
                                command.Parameters.AddWithValue("@Discount",        item.DiscountOnSale);
                                command.Parameters.AddWithValue("@Annotation",      item.AnnotationOnSale);
                                command.ExecuteNonQuery();
                            }
                            trans.Commit();
                            connection.Close();
                            return saleID;
                        };
                        connection.Close();
                        msgError = "No se pudo registrar la venta, intentelo otra vez.";
                        return 0;
                    }
                }
                catch (Exception ex)
                {
                    if (trans != null) trans.Rollback();
                    if (connection.State == ConnectionState.Open) connection.Close();
                    msgError = ex.Message;
                    return 0;
                }
            };
        }

        public int update(ref string msgError, string ID = null, string Status = null, string Annotation = null)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("saleUpdate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID",          ID);
                command.Parameters.AddWithValue("@Status",      Status);
                command.Parameters.AddWithValue("@Annotation",  Annotation);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public DataTable find(ref string msgError, int ID = 0, string Date = null)
        {
            if (Date == null)
            {
                Date = DateTime.Now.ToString("dd/MM/yyyy");
            }

            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("saleFind", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID",      ID);
                command.Parameters.AddWithValue("@DATE",    Date);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable table = new DataTable();
                adapter.Fill(table);

                return table;
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                DataTable table = new DataTable();
                return table;
            }
        }

        public DataTable findDetail(ref string msgError, int ID = 0)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("saleDetailFind", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@SaleID", ID);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable table = new DataTable();
                adapter.Fill(table);

                return table;
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                DataTable table = new DataTable();
                return table;
            }
        }
    }
}
