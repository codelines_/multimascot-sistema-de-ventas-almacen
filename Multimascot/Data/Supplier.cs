﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace Data
{
    public class Supplier
    {
        public int create(ref string msgError, Entity.Supplier supplier)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("supplierCreate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Description", supplier.Description);
                command.Parameters.AddWithValue("@RUC",         supplier.RUC);
                command.Parameters.AddWithValue("@Phone",       supplier.Phone);
                command.Parameters.AddWithValue("@Annotation",  supplier.Annotation);
                command.Parameters.AddWithValue("@Address",     supplier.Address);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public int update(ref string msgError, Entity.Supplier supplier)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("supplierUpdate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID",          supplier.ID);
                command.Parameters.AddWithValue("@Description", supplier.Description);
                command.Parameters.AddWithValue("@RUC",         supplier.RUC);
                command.Parameters.AddWithValue("@Phone",       supplier.Phone);
                command.Parameters.AddWithValue("@Annotation",  supplier.Annotation);
                command.Parameters.AddWithValue("@Address",     supplier.Address);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public List<Entity.Supplier> find(ref string msgError, int ID = 0)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            List<Entity.Supplier> list = new List<Entity.Supplier>();
            try
            {
                SqlCommand command = new SqlCommand("supplierFind", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID", ID);
                connection.Open();
                SqlDataReader dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Entity.Supplier item = new Entity.Supplier();
                        item.ID             = dr["ID"] as int? ?? 0;
                        item.Description    = dr["Description"] as string;
                        item.RUC            = dr["RUC"] as string;
                        item.Phone          = dr["Phone"] as string;
                        item.Annotation     = dr["Annotation"] as string;
                        item.Address        = dr["Address"] as string;
                        item.UpdatedAt      = Convert.ToDateTime(dr["UpdatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        item.CreatedAt      = Convert.ToDateTime(dr["CreatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        list.Add(item);
                    }
                }
                connection.Close();
                return list;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return list;
            }
        }

    }
}
