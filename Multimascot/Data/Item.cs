﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace Data
{
    public class Item
    {
        public int create(ref string msgError, Entity.Item item)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("itemCreate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Code",            item.Code);
                command.Parameters.AddWithValue("@Cost",            item.Cost);
                command.Parameters.AddWithValue("@Description",     item.Description);
                command.Parameters.AddWithValue("@DescriptionShort",item.DescriptionShort);
                command.Parameters.Add("@ImageSmall", SqlDbType.Image).Value    = item.ImageSmall;
                command.Parameters.Add("@ImageBig", SqlDbType.Image).Value      = item.ImageBig;


                if (item.SupplierID > 0)
                {
                    command.Parameters.AddWithValue("@SupplierID", item.SupplierID);
                }
                else
                {
                    command.Parameters.AddWithValue("@SupplierID", DBNull.Value);
                }

                if (item.CategoryID > 0)
                {
                    command.Parameters.AddWithValue("@CategoryID", item.CategoryID);
                }
                else
                {
                    command.Parameters.AddWithValue("@CategoryID", DBNull.Value);
                }

                command.Parameters.AddWithValue("@UnitPrice",       item.UnitPrice);
                command.Parameters.AddWithValue("@QuarterPrice",    item.QuarterPrice);
                command.Parameters.AddWithValue("@DozenPrice",      item.DozenPrice);
                command.Parameters.AddWithValue("@QuantityPerUnit", item.QuantityPerUnit);
                command.Parameters.AddWithValue("@UnitsInStock",    item.UnitsInStock);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public int update(ref string msgError, Entity.Item item)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("itemUpdate", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID",                  item.ID);
                command.Parameters.AddWithValue("@Code",                item.Code);
                command.Parameters.AddWithValue("@Cost",                item.Cost);
                command.Parameters.AddWithValue("@Description",         item.Description);
                command.Parameters.AddWithValue("@DescriptionShort",    item.DescriptionShort);
                command.Parameters.Add("@ImageSmall", SqlDbType.Image).Value    = item.ImageSmall;
                command.Parameters.Add("@ImageBig", SqlDbType.Image).Value      = item.ImageBig;

                if (item.SupplierID > 0)
                    command.Parameters.AddWithValue("@SupplierID",      item.SupplierID);

                if (item.CategoryID > 0)
                    command.Parameters.AddWithValue("@CategoryID",      item.CategoryID);

                command.Parameters.AddWithValue("@UnitPrice",           item.UnitPrice);
                command.Parameters.AddWithValue("@QuarterPrice",        item.QuarterPrice);
                command.Parameters.AddWithValue("@DozenPrice",          item.DozenPrice);
                command.Parameters.AddWithValue("@QuantityPerUnit",     item.QuantityPerUnit);
                command.Parameters.AddWithValue("@UnitsInStock",        item.UnitsInStock);
                command.Parameters.AddWithValue("@Discontinued",        item.Discontinued);
                connection.Open();
                int resp = command.ExecuteNonQuery();
                connection.Close();
                return resp;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return 0;
            }
        }

        public List<Entity.Item> find(ref string msgError, int ID = 0, string Description = null, int categoryID = 0)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            List<Entity.Item> list = new List<Entity.Item>();
            try
            {
                SqlCommand command = new SqlCommand("itemFind", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ID",          ID);
                command.Parameters.AddWithValue("@Description", Description);
                command.Parameters.AddWithValue("@categoryID",  categoryID);
                connection.Open();
                SqlDataReader dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Entity.Item item = new Entity.Item();
                        item.ID                 = dr["ID"] as int? ?? 0;
                        item.Code               = dr["Code"] as string;
                        item.Cost               = Convert.ToDouble(dr["Cost"]);
                        item.Description        = dr["Description"] as string;
                        item.DescriptionShort   = dr["DescriptionShort"] as string;
                        item.ImageSmall         = dr["ImageSmall"];
                        item.ImageBig           = dr["ImageBig"];
                        item.SupplierID         = dr["SupplierID"] as Int32? ?? 0;
                        item.CategoryID         = dr["CategoryID"] as Int32? ?? 0;
                        item.UnitPrice          = Convert.ToDouble(dr["UnitPrice"]);
                        item.QuarterPrice       = Convert.ToDouble(dr["QuarterPrice"]);
                        item.DozenPrice         = Convert.ToDouble(dr["DozenPrice"]);
                        item.QuantityPerUnit    = dr["QuantityPerUnit"] as string;
                        item.UnitsInStock       = dr["UnitsInStock"] as Int32? ?? 0;
                        item.Discontinued       = dr["Discontinued"] as bool? ?? false;

                        item.CategoryDescription = dr["CategoryDescription"] as string;
                        item.SupplierDescription = dr["SupplierDescription"] as string;
                        item.UpdatedAt          = Convert.ToDateTime(dr["UpdatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        item.CreatedAt          = Convert.ToDateTime(dr["CreatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                        list.Add(item);
                    }
                }
                connection.Close();
                return list;
            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                msgError = ex.Message;
                return list;
            }
        }

        public DataTable itemUnitsInStock(ref string msgError, int unitsInStock = 0)
        {
            SqlConnection connection = new SqlConnection(ConnectionStr.Multimascot);
            try
            {
                SqlCommand command = new SqlCommand("itemUnitsInStock", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@unitsInStock", unitsInStock);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable table = new DataTable();
                adapter.Fill(table);

                return table;
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                DataTable table = new DataTable();
                return table;
            }
        }
    }
}
