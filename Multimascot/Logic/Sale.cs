﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;
using Data;
using System.Data;

namespace Logic
{
    public class Sale
    {
        public int create(ref string msgError, Entity.Sale sale, List<Entity.Item> items)
        {
            return new Data.Sale().create(ref msgError, sale, items);
        }

        public DataTable find(ref string msgError, int ID = 0, string Date = null)
        {
            return new Data.Sale().find(ref msgError, ID, Date);
        }

        public DataTable findDetail(ref string msgError, int ID = 0)
        {
            return new Data.Sale().findDetail(ref msgError, ID);
        }


        public int update(ref string msgError, string ID = null, string Status = null, string Annotation = null)
        {
            return new Data.Sale().update(ref msgError, ID, Status, Annotation);
        }
    }
}
