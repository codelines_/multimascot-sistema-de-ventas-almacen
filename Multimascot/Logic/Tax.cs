﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;
using Data;
namespace Logic
{
    public class Tax
    {
        public int create(ref string msgError, Entity.Tax tax)
        {
            return new Data.Tax().create(ref msgError, tax);
        }

        public List<Entity.Tax> find(ref string msgError)
        {
            return new Data.Tax().find(ref msgError);
        }
    }
}
