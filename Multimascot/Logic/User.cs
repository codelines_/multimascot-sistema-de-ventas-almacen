﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;
using Data;

namespace Logic
{
    public class User
    {
        public int create(ref string msgError, Entity.User user)
        {
            return new Data.User().create(ref msgError, user);
        }

        public int update(ref string msgError, Entity.User user)
        {
            return new Data.User().update(ref msgError, user);
        }

        public List<Entity.User> find(ref string msgError, int ID = 0)
        {
            return new Data.User().find(ref msgError, ID);
        }

        public List<Entity.User> login(ref string msgError, string Account, string Password)
        {
            return new Data.User().login(ref msgError, Account, Password);
        }

        public int changePassword(ref string msgError, Entity.User user)
        {
            return new Data.User().changePassword(ref msgError, user);
        }
    }
}
