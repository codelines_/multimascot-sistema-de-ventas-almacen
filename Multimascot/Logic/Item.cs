﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace Logic
{
    public class Item
    {
        public int create(ref string msgError, Entity.Item item)
        {
            return new Data.Item().create(ref msgError, item);
        }

        public int update(ref string msgError, Entity.Item item)
        {
            return new Data.Item().update(ref msgError, item);
        }

        public List<Entity.Item> find(ref string msgError, int ID = 0, string Description = null, int categoryID = 0)
        {
            return new Data.Item().find(ref msgError, ID, Description, categoryID);
        }

        public DataTable itemUnitsInStock(ref string msgError, int unitsInStock = 0)
        {
            return new Data.Item().itemUnitsInStock(ref msgError, unitsInStock);
        }
    }
}
