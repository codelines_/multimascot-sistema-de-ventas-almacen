﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;
using Data;
namespace Logic
{
    public class Supplier
    {
        public int create(ref string msgError, Entity.Supplier supplier)
        {
            return new Data.Supplier().create(ref msgError, supplier);
        }

        public int update(ref string msgError, Entity.Supplier supplier)
        {
            return new Data.Supplier().update(ref msgError, supplier);
        }

        public List<Entity.Supplier> find(ref string msgError, int ID = 0)
        {
            return new Data.Supplier().find(ref msgError, ID);
        }
    }
}
