﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;
using Data;
namespace Logic
{
    public class Customer
    {
        public int create(ref string msgError, Entity.Customer customer)
        {
            return new Data.Customer().create(ref msgError, customer);
        }

        public int update(ref string msgError, Entity.Customer customer)
        {
            return new Data.Customer().update(ref msgError, customer);
        }

        public List<Entity.Customer> find(ref string msgError, int ID = 0, string Name = null)
        {
            return new Data.Customer().find(ref msgError, ID, Name);
        }

    }
}
