﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;
using Data;
namespace Logic
{
    public class Category
    {
        public int create(ref string msgError, Entity.Category category)
        {
            return new Data.Category().create(ref msgError, category);
        }

        public int update(ref string msgError, Entity.Category category)
        {
            return new Data.Category().update(ref msgError, category);
        }

        public List<Entity.Category> find(ref string msgError, int ID = 0)
        {
            return new Data.Category().find(ref msgError, ID);
        }
    }
}
