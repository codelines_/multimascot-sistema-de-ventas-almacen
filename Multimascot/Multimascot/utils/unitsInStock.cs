﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Logic;
using Entity;

namespace Multimascot.utils
{
    public partial class unitsInStock : Form
    {
        public unitsInStock()
        {
            InitializeComponent();
        }

        private void txtPercentage_KeyPress(object sender, KeyPressEventArgs e)
        {
          
        }

        int tmpUnitsInStock = 0;
        private void txtUnitsInStock_KeyUp(object sender, KeyEventArgs e)
        {
            string _unitsInStock = txtUnitsInStock.Text;

            if (!int.TryParse(_unitsInStock, out tmpUnitsInStock))
            {
                //MessageBox.Show("El valor debe ser un numero entero", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtUnitsInStock.Text = "0";
                dataGrid.DataSource = null;
                return;
            }

            string msgError = "";
            DataTable data = new Logic.Item().itemUnitsInStock(ref msgError, tmpUnitsInStock);

            dataGrid.DataSource = data;
        }

        private void unitsInStock_Load(object sender, EventArgs e)
        {

        }

        private void btnCsv_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "stock menores o iguales a " + tmpUnitsInStock.ToString() + ".xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                DataGridView dgCopy = dataGrid;
                new Multimascot.libs.csv().ToCsV(dataGrid, sfd.FileName);
            }
        }
    }
}
