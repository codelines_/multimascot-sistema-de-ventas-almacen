﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;

namespace Multimascot.maintenance
{
    public partial class customer : Form
    {
        public customer()
        {
            InitializeComponent();
        }

        private void customer_Load(object sender, EventArgs e)
        {
            fillGrid();
        }

        private void fillGrid()
        {
            string filter = null;
            if (txtFilterName.Text.Trim().Length > 0)
            {
                filter = txtFilterName.Text.Trim();
            }

            var msgError = "";
            var founds = new Logic.Customer().find(ref msgError, 0, filter);
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            dataGrid.DataSource = founds;
 
        }

        private Tuple<bool, Entity.Customer> dataIsValid()
        {
            Entity.Customer entity = new Entity.Customer();

            string name = txtName.Text.Trim();
            if (name.Length == 0)
            {
                MessageBox.Show("El campo «Nombre» no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            entity.Name         = name;
            entity.RUC          = txtRUC.Text.Trim();
            entity.Phone        = txtPhone.Text.Trim();
            entity.Annotation   = txtAnnotation.Text.Trim();
            entity.Email        = txtEmail.Text.Trim();
            entity.Address      = txtAddress.Text.Trim();

            return Tuple.Create(true, entity);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de registrar este Cliente?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            var result = dataIsValid();
            if (!result.Item1)
            {
                return;
            }

            Entity.Customer customer = result.Item2;
            string msgError = "";
            int records = new Logic.Customer().create(ref msgError, customer);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("El Cliente ha sido agregado con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

            txtName.Clear();
            txtRUC.Clear();
            txtPhone.Clear();
            txtAnnotation.Clear();
            txtEmail.Clear();
            txtAddress.Clear();
            
            fillGrid();
        }

        private void dataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int ID = 0;
            try
            {
                ID = (int)dataGrid.Rows[e.RowIndex].Cells[0].Value;
            }
            catch (Exception) { return; }

            string msgError = "";
            List<Entity.Customer> customer = new Logic.Customer().find(ref msgError, ID);
            if (customer.Count > 0)
            {
                maintenance.customer_update form = new customer_update(customer.First());
                var response = form.ShowDialog();
                if (response == DialogResult.OK)
                {
                    fillGrid();
                }
            }
        }

        private void txtFilterName_KeyUp(object sender, KeyEventArgs e)
        {
            fillGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "clientes.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                new Multimascot.libs.csv().ToCsV(dataGrid, sfd.FileName);
            }
        }
    }
}
