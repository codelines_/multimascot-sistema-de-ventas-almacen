﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;

namespace Multimascot.maintenance
{
    public partial class user_update : Form
    {
        Entity.User user;
        public user_update(Entity.User _user)
        {
            InitializeComponent();
            user = _user;
        }

        private void user_update_Load(object sender, EventArgs e)
        {
            txtName.Text            = user.Name;
            txtAccount.Text         = user.Account;
            txtIdentityCard.Text    = user.IdentityCard;
            txtPhone.Text           = user.Phone;
            chkStatus.Checked       = user.Status;
            chkAdmin.Checked        = user.Admin;
            txtCreatedAt.Text       = user.CreatedAt;
            txtUpdatedAt.Text       = user.UpdatedAt;
        }

        private Tuple<bool, Entity.User> dataIsValid()
        {
            Entity.User entity = new Entity.User();

            string name = txtName.Text.Trim();
            if (name.Length == 0)
            {
                MessageBox.Show("El campo Nombre no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            entity.Name = name;

            string account = txtAccount.Text.Trim();
            if (account.Length == 0)
            {
                MessageBox.Show("El campo Cuenta no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            entity.Account = account;

            if (txtPassword.Enabled)
            {
                string password = txtPassword.Text;
                if (password.Length == 0)
                {
                    MessageBox.Show("El campo Contraseña no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return Tuple.Create(false, entity);
                }
                entity.Password = password;
            }

            string identityCard = txtIdentityCard.Text.Trim();
            if (identityCard.Length > 0)
            {
                entity.IdentityCard = identityCard;
            }

            string phone = txtPhone.Text.Trim();
            if (phone.Length > 0)
            {
                entity.Phone = phone;
            }

            entity.ID       = user.ID;
            entity.Status   = chkStatus.Checked;
            entity.Admin    = chkAdmin.Checked;

            return Tuple.Create(true, entity);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de modificar este Usuario?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            var result = dataIsValid();
            if (!result.Item1)
            {
                return;
            }

            Entity.User usuario = result.Item2;
            string msgError = "";
            int records = new Logic.User().update(ref msgError, usuario);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("El Usuario ha sido modificado con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
        }

        private void chkNewPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
            {
                txtPassword.Enabled = true;
            }
            else
            {
                txtPassword.Enabled = false;
                txtPassword.Clear();
            }
        }

        private void user_update_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
