﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;

namespace Multimascot.maintenance
{
    public partial class supplier : Form
    {
        public supplier()
        {
            InitializeComponent();
        }

        private void supplier_Load(object sender, EventArgs e)
        {
            fillGrid();
        }

        private void fillGrid()
        {
            var msgError = "";
            var founds = new Logic.Supplier().find(ref msgError);
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            dataGrid.DataSource = founds;
        }

        private Tuple<bool, Entity.Supplier> dataIsValid()
        {
            Entity.Supplier entity = new Entity.Supplier();

            string description = txtDescription.Text.Trim();
            if (description.Length == 0)
            {
                MessageBox.Show("El campo Descripcion no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            entity.Description  = description;
            entity.RUC          = txtRUC.Text.Trim();
            entity.Phone        = txtPhone.Text.Trim();
            entity.Annotation   = txtAnnotation.Text.Trim();
            entity.Address      = txtAddress.Text.Trim();
          
            return Tuple.Create(true, entity);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de registrar este Proveedor?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            var result = dataIsValid();
            if (!result.Item1)
            {
                return;
            }

            Entity.Supplier supplier = result.Item2;
            string msgError = "";
            int records = new Logic.Supplier().create(ref msgError, supplier);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("El proveedor ha sido agregado con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtDescription.Clear();
            txtRUC.Clear();
            txtPhone.Clear();
            txtAnnotation.Clear();
            txtAddress.Clear();
            
            fillGrid();
        }


        private void dataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int ID = 0;
            try
            {
                ID = (int)dataGrid.Rows[e.RowIndex].Cells[0].Value;
            }
            catch (Exception) { return; }

            string msgError = "";
            List<Entity.Supplier> supplier = new Logic.Supplier().find(ref msgError, ID);
            if (supplier.Count > 0)
            {
                maintenance.supplier_update form = new supplier_update(supplier.First());
                var response = form.ShowDialog();
                if (response == DialogResult.OK)
                {
                    fillGrid();
                }
            }
        }

        private void btnCsv_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "proveedores.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                new Multimascot.libs.csv().ToCsV(dataGrid, sfd.FileName);
            }
        }
    }
}
