﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Multimascot.maintenance
{
    public partial class tax : Form
    {
        public tax()
        {
            InitializeComponent();
        }

        private void tax_Load(object sender, EventArgs e)
        {
            fillGrid();
        }

        private void fillGrid()
        {
            var msgError = "";
            var founds = new Logic.Tax().find(ref msgError);
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            dataGrid.DataSource = founds;
        }

        private Tuple<bool, Entity.Tax> dataIsValid()
        {
            Entity.Tax entity = new Entity.Tax();

            string percentage = txtPercentage.Text.Trim();
            if (percentage.Length == 0)
            {
                MessageBox.Show("El campo Valor% no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            double _percentaje = 0;
            if (!Double.TryParse(percentage, out _percentaje))
            {
                MessageBox.Show("El campo Valor% debe ser un numero decimal", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            entity.Percentage = _percentaje;

            return Tuple.Create(true, entity);
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de registrar este valor de impuesto?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            var result = dataIsValid();
            if (!result.Item1)
            {
                return;
            }

            Entity.Tax tax = result.Item2;
            string msgError = "";
            int records = new Logic.Tax().create(ref msgError, tax);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("El valor de impuesto ha sido agregado con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtPercentage.Clear();
            fillGrid();
        }

    }
}
