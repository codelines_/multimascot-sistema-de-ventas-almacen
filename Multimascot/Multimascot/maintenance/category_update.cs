﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;

namespace Multimascot.maintenance
{
    public partial class category_update : Form
    {
        Entity.Category category;
        public category_update(Entity.Category _category)
        {
            InitializeComponent();
            category = _category;
        }

        private void category_update_Load(object sender, EventArgs e)
        {
            txtDescription.Text = category.Description;
            txtCreatedAt.Text   = category.CreatedAt;
            txtUpdatedAt.Text   = category.UpdatedAt;
        }

        private Tuple<bool, Entity.Category> dataIsValid()
        {
            Entity.Category entity = new Entity.Category();

            string description = txtDescription.Text.Trim();
            if (description.Length == 0)
            {
                MessageBox.Show("El campo Descripcion no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            entity.ID           = category.ID;
            entity.Description  = description;

            return Tuple.Create(true, entity);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de modificar esta Categoria?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            var result = dataIsValid();
            if (!result.Item1)
            {
                return;
            }

            Entity.Category entity = result.Item2;
            string msgError = "";
            int records = new Logic.Category().update(ref msgError, entity);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("La categoria ha sido modificada con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
        }

        private void category_update_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
