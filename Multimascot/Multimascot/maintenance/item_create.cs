﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;
using System.IO;
using System.Drawing.Drawing2D;

namespace Multimascot.maintenance
{
    public partial class item_create : Form
    {
        public item_create()
        {
            InitializeComponent();
        }

        private Tuple<bool, Entity.Item> dataIsValid()
        {
            Entity.Item entity = new Entity.Item();

            #region validationImage
            string imagePath = txtImage.Text.Trim();
            if (imagePath.Length > 0)
            {
                byte[] imageDataSmall;
                byte[] imageDataBig;
                try
                {
                    //Read Image Bytes into a byte array
                    imageDataSmall = ReadFile(imagePath, 75);
                    imageDataBig = ReadFile(imagePath, 200);
                }
                catch (Exception)
                {
                    MessageBox.Show("La «imagen» del producto no es valida", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return Tuple.Create(false, entity);
                }
                entity.ImageSmall = imageDataSmall;
                entity.ImageBig = imageDataBig;
            }
            #endregion

            #region validationDescription
            string description = txtDescription.Text.Trim();
            if (description.Length == 0)
            {
                MessageBox.Show("El campo «Descripcion» no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            entity.Description = description;
            #endregion

            #region validationDescriptionShort
            string descriptionShort = txtDescriptionShort.Text.Trim();
            if (descriptionShort.Length == 0)
            {
                MessageBox.Show("El campo «Descripcion corta» no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            entity.DescriptionShort = descriptionShort;
            #endregion

            #region validationUnitPrice
            string unitPrice = txtUnitPrice.Text.Trim();
            if (unitPrice.Length == 0)
            {
                MessageBox.Show("El campo «Precio unitario» no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            double _unitPrice = 0;
            if (!Double.TryParse(unitPrice, out _unitPrice))
            {
                MessageBox.Show("El campo «Precio unitario» debe ser un numero decimal", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            entity.UnitPrice = _unitPrice;
            #endregion

            #region validationQuarterPrice
            string quarterPrice = txtQuarterPrice.Text.Trim();
            if (quarterPrice.Length == 0)
            {
                MessageBox.Show("El campo «Precio por cuarto» no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            double _quarterPrice = 0;
            if (!Double.TryParse(quarterPrice, out _quarterPrice))
            {
                MessageBox.Show("El campo «Precio por cuarto» debe ser un numero decimal", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            entity.QuarterPrice = _quarterPrice;
            #endregion

            #region validationDozenPrice
            string dozenPrice = txtDozenPrice.Text.Trim();
            if (dozenPrice.Length == 0)
            {
                MessageBox.Show("El campo «Precio por docena» no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            double _dozenPrice = 0;
            if (!Double.TryParse(dozenPrice, out _dozenPrice))
            {
                MessageBox.Show("El campo «Precio por docena» debe ser un numero decimal", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            entity.DozenPrice = _dozenPrice;
            #endregion

            #region validationUnitsInStock
            string unitsInStock = txtUnitsInStock.Text.Trim();
            if (unitsInStock.Length == 0)
            {
                MessageBox.Show("El campo «Unidades en stock» no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            int _unitsInStock = 0;
            if (!Int32.TryParse(unitsInStock, out _unitsInStock))
            {
                MessageBox.Show("El campo «Unidades en stock» debe ser un numero entero", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            entity.UnitsInStock = _unitsInStock;
            #endregion

            // Pass
            #region validationCode
            string code = txtCode.Text.Trim();
            if (code.Length > 0)
            {
                entity.Code = code;
            }
            #endregion

            #region validationCost
            string cost = txtCost.Text.Trim();
            if (cost.Length > 0)
            {
                double _cost = 0;
                if (!Double.TryParse(cost, out _cost)) {
                    MessageBox.Show("El campo «Costo» debe ser un numero decimal", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return Tuple.Create(false, entity);
                }

                entity.Cost = _cost;
            }
            #endregion

            #region validationQuantityPerUnit
            string quantityPerUnit = txtQuantityPerUnit.Text.Trim();
            if (quantityPerUnit.Length > 0)
            {
                entity.QuantityPerUnit = quantityPerUnit;
            }
            #endregion

            entity.SupplierID = Convert.ToInt32(cmbSupplier.SelectedValue);
            entity.CategoryID = Convert.ToInt32(cmbCategory.SelectedValue);

            return Tuple.Create(true, entity);
        }

        public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return newImage;
        }

        byte[] ReadFile(string sPath, int size)
        {
            MemoryStream ms = new MemoryStream();
            Image img = Image.FromFile(sPath);
            var newImage = ScaleImage(img, size, size);
            newImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de registrar este Producto?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            var result = dataIsValid();
            if (!result.Item1)
            {
                return;
            }

            Entity.Item item = result.Item2;
            string msgError = "";
            int records = new Logic.Item().create(ref msgError, item);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("El Producto ha sido agregado con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
        }

        private void btnSelectImage_Click(object sender, EventArgs e)
        {
            //Ask user to select file.
            OpenFileDialog dlg = new OpenFileDialog();
            DialogResult dlgRes = dlg.ShowDialog();
            if (dlgRes != DialogResult.Cancel)
            {
                //Set image in picture box
                picImagePreview.ImageLocation = dlg.FileName;

                //Provide file path in txtImagePath text box.
                txtImage.Text = dlg.FileName;
            }
        }

        private void btnDeleteImage_Click(object sender, EventArgs e)
        {
            picImagePreview.ImageLocation = null;
            txtImage.Clear();
        }

        private void Item_create_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void fillCategory()
        {
            var msgError    = "";
            var founds      = new Logic.Category().find(ref msgError);
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var unspecified = new Entity.Category() { ID = 0, Description = "(No especificado)" };

            List<Entity.Category> reOrdered = new List<Entity.Category>();
            reOrdered.Add(unspecified);

            foreach (var item in founds)
            {
                reOrdered.Add(item);
            }

            cmbCategory.DataSource      = reOrdered;
            cmbCategory.DisplayMember   = "Description";
            cmbCategory.ValueMember     = "ID";
        }

        private void fillSupplier()
        {
            var msgError    = "";
            var founds      = new Logic.Supplier().find(ref msgError);
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var unspecified = new Entity.Supplier() { ID = 0, Description = "(No especificado)" };

            List<Entity.Supplier> reOrdered = new List<Entity.Supplier>();
            reOrdered.Add(unspecified);

            foreach (var item in founds)
            {
                reOrdered.Add(item);
            }

            cmbSupplier.DataSource      = reOrdered;
            cmbSupplier.DisplayMember   = "Description";
            cmbSupplier.ValueMember     = "ID";
        }

        private void Item_create_Load(object sender, EventArgs e)
        {
            fillSupplier();
            fillCategory();
        }
    }
}
