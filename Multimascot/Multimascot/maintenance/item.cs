﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;
using System.IO;

namespace Multimascot.maintenance
{
    public partial class item : Form
    {
        public item()
        {
            InitializeComponent();
        }

        private void Item_Load(object sender, EventArgs e)
        {
            fillCategory();
            fillGrid();
        }

        private void fillCategory()
        {
            var msgError = "";
            var founds = new Logic.Category().find(ref msgError);
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var unspecified = new Entity.Category() { ID = 0, Description = "(No especificado)" };

            List<Entity.Category> reOrdered = new List<Entity.Category>();
            reOrdered.Add(unspecified);

            foreach (var item in founds)
            {
                reOrdered.Add(item);
            }

            cmbCategory.DataSource = reOrdered;
            cmbCategory.DisplayMember = "Description";
            cmbCategory.ValueMember = "ID";
        }

        private void fillGrid(string Description = null)
        {
            var msgError = "";
            int categoryID = Convert.ToInt32(cmbCategory.SelectedValue);

            List<Entity.Item> founds = new Logic.Item().find(ref msgError, 0, txtFilterDescription.Text.Trim(), categoryID);
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var edited = founds.Select(x => new
            {
                ID = x.ID,
                x.Code,
                x.Cost,
                x.Description,
                x.DescriptionShort,
                ImageSmall = x.ImageSmall,
                x.SupplierDescription,
                x.CategoryDescription,
                x.UnitPrice,
                x.QuarterPrice,
                x.DozenPrice,
                x.QuantityPerUnit,
                x.UnitsInStock,
                Discontinued = (x.Discontinued) ? "Descontinuado" : "Activo",
                x.UpdatedAt,
                x.CreatedAt
            }).ToList();

            dataGrid.DataSource = edited;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            item_create form = new item_create();
            var response = form.ShowDialog();
            if (response == DialogResult.OK)
            {
                fillGrid();
            }
        }

        private void dataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int ID = 0;
            try
            {
                ID = (int)dataGrid.Rows[e.RowIndex].Cells[0].Value;
            }
            catch (Exception) { return; }

            string msgError = "";
            List<Entity.Item> item = new Logic.Item().find(ref msgError, ID);
            if (item.Count > 0)
            {
                maintenance.item_update form = new item_update(item.First());
                var response = form.ShowDialog();
                if (response == DialogResult.OK)
                {
                    fillGrid();
                }
            }
        }

        private void txtFilterDescription_KeyUp(object sender, KeyEventArgs e)
        {
            string filter = txtFilterDescription.Text.Trim();
            fillGrid(filter);
        }

        private void cmbCategory_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string filter = txtFilterDescription.Text.Trim();
            fillGrid(filter);
        }

        private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnCsv_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "productos.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                DataGridView dgCopy = dataGrid;
                new Multimascot.libs.csv().ToCsV(dataGrid, sfd.FileName);
            }
        }
    }
}
