﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Multimascot.maintenance
{
    public partial class sale : Form
    {
        public sale()
        {
            InitializeComponent();
        }

        private void sale_Load(object sender, EventArgs e)
        {
            fillGrid();
        }

        private void dataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int ID = 0;
            try
            {
                ID = (int)dataGrid.Rows[e.RowIndex].Cells[0].Value;
            }
            catch (Exception) { return; }

            string msgError = "";
            var sales = new Logic.Sale().find(ref msgError, ID);
            if (sales.Rows.Count > 0)
            {
                maintenance.sale_update form = new sale_update(sales.Rows[0]);
                var response = form.ShowDialog();
                if (response == DialogResult.OK)
                {
                    fillGrid();
                }
            }
        }

        private void fillGrid()
        {
            var msgError = "";
            var founds = new Logic.Sale().find(ref msgError, 0, dtPick.Value.ToString("dd/MM/yyyy"));
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            dataGrid.DataSource = founds;

            double total = 0;
            foreach (DataRow item in founds.Rows)
            {
                if (item["Status"].ToString() == "activo")
                {
                    total += Convert.ToDouble(item["Total"]);
                }
            }

            lblSumTotal.Text = "Total del dia: S/. " + total.ToString("F");
        }

        private void dtPick_ValueChanged(object sender, EventArgs e)
        {
            fillGrid();
        }

        private void btnCsv_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "ventas del dia - " + dtPick.Value.ToString("dd-MM-yyyy") + ".xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                DataGridView dgCopy = dataGrid;
                new Multimascot.libs.csv().ToCsV(dataGrid, sfd.FileName);
            }
        }

    }
}
