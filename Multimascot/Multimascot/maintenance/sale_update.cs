﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Multimascot.maintenance
{
    public partial class sale_update : Form
    {
        DataRow sale;
        public sale_update(DataRow _sale)
        {
            InitializeComponent();
            sale = _sale;
        }

        private void sale_update_Load(object sender, EventArgs e)
        {
            txtClient.Text = sale["Name"].ToString();
            txtUser.Text = sale["Account"].ToString();
            txtDate.Text = sale["Date"].ToString();
            txtCreatedAt.Text = sale["CreatedAt"].ToString();
            txtUpdatedAt.Text = sale["UpdatedAt"].ToString();
            txtAnnotation.Text = sale["Annotation"].ToString();

            txtSubTotal.Text = sale["SubTotal"].ToString();
            txtDiscount.Text = sale["Discount"].ToString();
            txtTax.Text = sale["IGV"].ToString();
            txtTotal.Text = sale["Total"].ToString();

            if (sale["Status"].ToString() == "activo")
            {
                chkActive.Checked = true;
            }

            string msgError = "";
            DataTable details = new Logic.Sale().findDetail(ref msgError, Convert.ToInt32(sale["ID"]));
            dataGrid.DataSource = details;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de modificar esta Categoria?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            string status = "activo";
            if (!chkActive.Checked)
            {
                status = "cancelado";   
            }

            string msgError = "";
            int records = new Logic.Sale().update(ref msgError, sale["ID"].ToString(), status, txtAnnotation.Text.Trim());

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("La categoria ha sido modificada con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            report.saleDetail form = new report.saleDetail((Int32)sale["ID"]);
            form.ShowDialog();
        }
    }
}
