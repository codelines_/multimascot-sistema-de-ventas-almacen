﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Multimascot.maintenance
{
    public partial class customer_update : Form
    {
        Entity.Customer customer;
        public customer_update(Entity.Customer _customer)
        {
            InitializeComponent();
            customer = _customer;
        }

        private void customer_update_Load(object sender, EventArgs e)
        {
            txtName.Text        = customer.Name;
            txtRUC.Text         = customer.RUC;
            txtPhone.Text       = customer.Phone;
            txtAnnotation.Text  = customer.Annotation;
            txtCreatedAt.Text   = customer.CreatedAt;
            txtUpdatedAt.Text   = customer.UpdatedAt;
            txtEmail.Text       = customer.Email;
            txtAddress.Text     = customer.Address;
        }

        private Tuple<bool, Entity.Customer> dataIsValid()
        {
            Entity.Customer entity = new Entity.Customer();

            string name = txtName.Text.Trim();
            if (name.Length == 0)
            {
                MessageBox.Show("El campo «Nombre» no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            entity.ID           = customer.ID;
            entity.Name         = name;
            entity.RUC          = txtRUC.Text.Trim();
            entity.Phone        = txtPhone.Text.Trim();
            entity.Annotation   = txtAnnotation.Text.Trim();
            entity.Email        = txtEmail.Text.Trim();
            entity.Address      = txtAddress.Text.Trim();

            return Tuple.Create(true, entity);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de modificar este Cliente?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            var result = dataIsValid();
            if (!result.Item1)
            {
                return;
            }

            Entity.Customer entity = result.Item2;
            string msgError = "";
            int records = new Logic.Customer().update(ref msgError, entity);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("El Cliente ha sido modificado con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
        }

        private void customer_update_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
