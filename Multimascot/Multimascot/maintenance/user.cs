﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;

namespace Multimascot.maintenance
{
    public partial class user : Form
    {
        public user()
        {
            InitializeComponent();
        }

        private void user_Load(object sender, EventArgs e)
        {
            fillGrid();
        }

        private void fillGrid()
        {
            var msgError = "";
            var founds = new Logic.User().find(ref msgError);
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var edited = founds.Select(x => new
            {
                ID = x.ID,
                x.Name,
                x.IdentityCard,
                x.Phone,
                x.Account,
                Status  = (x.Status) ? "Activo" : "No activo",
                Admin   = (x.Admin) ? "Si" : "No",
                x.CreatedAt,
                x.UpdatedAt
            }).ToList();

            dataGrid.DataSource = edited;
        }

        private Tuple<bool, Entity.User> dataIsValid()
        {
            Entity.User entity = new Entity.User();

            string name = txtName.Text.Trim();
            if (name.Length == 0)
            {
                MessageBox.Show("El campo Nombre no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            entity.Name = name;

            string account = txtAccount.Text.Trim();
            if (account.Length == 0)
            {
                MessageBox.Show("El campo Cuenta no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            entity.Account = account;

            string password = txtPassword.Text;
            if (password.Length == 0)
            {
                MessageBox.Show("El campo Contraseña no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }
            entity.Password = password;

            string identityCard = txtIdentityCard.Text.Trim();
            if (identityCard.Length > 0)
            {
                entity.IdentityCard = identityCard;
            }

            string phone = txtPhone.Text.Trim();
            if (phone.Length > 0)
            {
                entity.Phone = phone;
            }

            entity.Status = true;
            entity.Admin = chkAdmin.Checked;

            return Tuple.Create(true, entity);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de registrar este Usuario?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            var result = dataIsValid();
            if (!result.Item1)
            {
                return;
            }

            Entity.User user = result.Item2;
            string msgError = "";
            int records = new Logic.User().create(ref msgError, user);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("El Usuario ha sido agregado con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtAccount.Clear();
            txtPassword.Clear();
            txtName.Clear();
            txtIdentityCard.Clear();
            txtPhone.Clear();
            chkAdmin.Checked = false;
            
            fillGrid();
        }

        private void dataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int ID = 0;
            try
            {
                ID = (int)dataGrid.Rows[e.RowIndex].Cells[0].Value;
            }
            catch (Exception) { return; }

            string msgError = "";
            List<Entity.User> user = new Logic.User().find(ref msgError, ID);
            if (user.Count > 0)
            {
                maintenance.user_update form = new user_update(user.First());
                var response = form.ShowDialog();
                if (response == DialogResult.OK)
                {
                    fillGrid();
                }
            }
        }
    }
}
