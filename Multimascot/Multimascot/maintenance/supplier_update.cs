﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;

namespace Multimascot.maintenance
{
    public partial class supplier_update : Form
    {
        Entity.Supplier supplier;
        public supplier_update(Entity.Supplier _supplier)
        {
            InitializeComponent();
            supplier = _supplier;
        }

        private void supplier_update_Load(object sender, EventArgs e) {
            txtDescription.Text = supplier.Description;
            txtRUC.Text         = supplier.RUC;
            txtPhone.Text       = supplier.Phone;
            txtAnnotation.Text  = supplier.Annotation;
            txtCreatedAt.Text   = supplier.CreatedAt;
            txtUpdatedAt.Text   = supplier.UpdatedAt;
            txtAddress.Text     = supplier.Address;
        }

        private Tuple<bool, Entity.Supplier> dataIsValid()
        {
            Entity.Supplier entity = new Entity.Supplier();

            string description = txtDescription.Text.Trim();
            if (description.Length == 0)
            {
                MessageBox.Show("El campo Descripcion no debe estar vacio", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Tuple.Create(false, entity);
            }

            entity.ID           = supplier.ID;
            entity.Description  = description;
            entity.RUC          = txtRUC.Text.Trim();
            entity.Phone        = txtPhone.Text.Trim();
            entity.Annotation   = txtAnnotation.Text.Trim();
            entity.Address      = txtAddress.Text.Trim();

            return Tuple.Create(true, entity);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de modificar este Proveedor?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            var result = dataIsValid();
            if (!result.Item1)
            {
                return;
            }

            Entity.Supplier entity = result.Item2;
            string msgError = "";
            int records = new Logic.Supplier().update(ref msgError, entity);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("El proveedor ha sido modificado con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
        }

        private void supplier_update_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
