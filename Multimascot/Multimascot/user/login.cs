﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;
namespace Multimascot.user
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        public static Entity.User logged;
        private void btnEnter_Click(object sender, EventArgs e)
        {
            enter();
        }

        private void txtAccount_KeyUp(object sender, KeyEventArgs e)
        {
        }

        private void enter()
        {
            string msgError = "";
            List<Entity.User> users = new Logic.User().login(ref msgError, txtAccount.Text, txtPassword.Text);

            if (users.Count > 0)
            {
                logged = users[0];

                if (!logged.Status)
                {
                    MessageBox.Show("Su cuenta ha sido bloqueada, consulte con el administrador para mas información.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                txtPassword.Clear();
                user.menu form = new menu();

                form.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Usuario o contraseña no valido", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtPassword.Clear();
                txtAccount.Focus();
            }
        }

        private void txtAccount_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == 13)
            {
                enter();
            }
        }
    }
}
