﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Multimascot.user
{
    public partial class changePassword : Form
    {
        public changePassword()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro/a de cambiar su contraseña?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            if (txtOldPassword.Text != user.login.logged.Password)
            {
                MessageBox.Show("Debe ingresar su actual contraseña para continuar.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (txtNewPassword.Text != txtRepeatPassword.Text)
            {
                MessageBox.Show("Las contraseñas no coinciden.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            user.login.logged.Password = txtNewPassword.Text;

            string msgError = "";
            int records = new Logic.User().changePassword(ref msgError, user.login.logged);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show("Su contraseña ha sido cambiada con exito.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();

        }
    }
}
