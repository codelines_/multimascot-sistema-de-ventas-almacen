﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Multimascot.user
{
    public partial class menu : Form
    {
        public menu()
        {
            InitializeComponent();
        }

        private void menu_Load(object sender, EventArgs e)
        {
            lblStatus.Text = "Usuario: " + user.login.logged.Account;

            menuUser();
            if (user.login.logged.Admin)
            {
                maintenance();
            }
            saleMenu();
        }

        private void menuUser ()
        {
            var menu = new System.Windows.Forms.ToolStripMenuItem()
            {
                Text = "Usuario" 
            };

            var subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "Cambiar contraseña";
            subItem.Tag = "changePassword";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);

            subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "Cerrar sesión";
            subItem.Tag = "exit";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);

            menuStrip1.Items.Add(menu);
        }

        private void maintenance()
        {
            var menu = new System.Windows.Forms.ToolStripMenuItem()
            {
                Text = "Mantenimiento"
            };

            var subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "Categorias";
            subItem.Tag = "category";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);

            subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "Proveedores";
            subItem.Tag = "supplier";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);

            subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "Productos";
            subItem.Tag = "item";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);

            subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "Clientes";
            subItem.Tag = "customer";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);

            subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "Usuarios";
            subItem.Tag = "user";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);

            subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "Ventas";
            subItem.Tag = "sale";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);

            subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "Unidades en Stock";
            subItem.Tag = "unitsInStock";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);

            subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "IGV";
            subItem.Tag = "tax";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);


            menuStrip1.Items.Add(menu);
        }

        private void saleMenu()
        {
            var menu = new System.Windows.Forms.ToolStripMenuItem()
            {
                Text = "Ventas"
            };

            var subItem = new System.Windows.Forms.ToolStripMenuItem();
            subItem.Text = "Vender";
            subItem.Tag = "sale.sale";
            subItem.Click += new EventHandler(MenuItem_Click);
            menu.DropDownItems.Add(subItem);

            menuStrip1.Items.Add(menu);
        }

        private void MenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            string arg = menuItem.Tag.ToString();

            Form form = new Form();
            switch (arg)
            {
                case "changePassword":
                    form = new changePassword();
                    break;
                case "exit":
                    Application.Exit();
                    return;

                case "category":
                    form = new maintenance.category();
                    break;
                case "supplier":
                    form = new maintenance.supplier();
                    break;
                case "item":
                    form = new maintenance.item();
                    break;
                case "customer":
                    form = new maintenance.customer();
                    break;
                case "user":
                    form = new maintenance.user();
                    break;
                case "sale":
                    form = new maintenance.sale();
                    break;
                case "unitsInStock":
                    form = new utils.unitsInStock();
                    break;
                case "tax":
                    form = new maintenance.tax();
                    break;


                case "sale.sale":
                    form = new sale.sale();
                    break;
                default:
                    MessageBox.Show("Opcion no valida");
                    return;
            }

            form.MdiParent = this;
            form.Show();

        }
    }
}
