﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;
using Microsoft.Reporting.WinForms;
namespace Multimascot.report
{
    public partial class saleDetail : Form
    {
        int saleID = 0;
        public saleDetail(int _saleID)
        {
            InitializeComponent();
            saleID = _saleID;
        }

        private void saleDetail_Load(object sender, EventArgs e)
        {

            string msgError = "";

            var founds = new Logic.Sale().find(ref msgError, saleID, null);

            if (founds.Rows.Count == 0)
            {
                MessageBox.Show("No se encontro la venta", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow sale = founds.Rows[0];

            string client = sale["Name"].ToString();
            DateTime date = Convert.ToDateTime(sale["Date"].ToString());
            string day = date.Day.ToString();
            string month = date.Month.ToString();
            string year = date.Year.ToString();
            string identificationCard = sale["RUC"].ToString();
            string documentType = identificationCard.Length > 8 ? "RUC" : "DNI";


            DataTable details = new Logic.Sale().findDetail(ref msgError, saleID);

            DataTable data = new DataTable();
            data.Columns.Add("quantity", typeof(Int32));
            data.Columns.Add("description");
            data.Columns.Add("unitPrice", typeof(double));
            data.Columns.Add("total", typeof(double));
            data.Columns.Add("discount", typeof(double));
            data.Columns.Add("subTotal", typeof(double));
            data.Columns.Add("igv", typeof(double));
            data.Columns.Add("code");
            data.Columns.Add("quantityPerUnit");
            data.Columns.Add("descriptionShort");

            foreach (DataRow item in details.Rows)
            {
                DataRow dr = data.NewRow();
                dr["quantity"] = item["Cantidad"];
                dr["description"] = item["Nombre"];
                dr["descriptionShort"] = item["Descripcion"];
                dr["unitPrice"] = item["Precio de venta"];
                dr["total"] = item["Total"];
                dr["discount"] = item["Descuento"];
                dr["subTotal"] = item["SubTotal"];
                dr["igv"] = item["IGV"];
                dr["code"] = item["Code"];
                dr["quantityPerUnit"] = item["Talla"];
                data.Rows.Add(dr);
            }


            reportViewer1.Visible = true;
            reportViewer1.LocalReport.DataSources.Clear();

            reportViewer1.LocalReport.ReportPath = Application.StartupPath + @"\report\rpt\saleDetail.rdlc"; // creo q asi era :s@"C:\saleDetail.rdlc";

            ReportParameter[] parameters = new ReportParameter[6];
            parameters[0] = new ReportParameter("client", client);
            parameters[1] = new ReportParameter("day", day);
            parameters[2] = new ReportParameter("month", month);
            parameters[3] = new ReportParameter("year", year);
            parameters[4] = new ReportParameter("identificationCard", identificationCard);
            parameters[5] = new ReportParameter("documentType", documentType);


            reportViewer1.LocalReport.SetParameters(parameters);

            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("rpts", data));
            this.reportViewer1.RefreshReport();
        }
    }
}
