﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;
using System.IO;

namespace Multimascot.sale
{
    public partial class item : Form
    {
        Entity.Item EItem = new Entity.Item();
        public item()
        {
            InitializeComponent();
        }

        private class listPrecio
        {
            public string ID { get; set; }
            public string Description { get; set; }
        }
        double IGV = 0;
        private void Item_Load(object sender, EventArgs e)
        {
            //get Tax:last
            string msgErrorx = "";
            var taxes = new Logic.Tax().find(ref msgErrorx);
            if (taxes.Count > 0)
            {
                IGV = taxes[0].Percentage; //assuming that this in descending order by creation date
            }

            lblIGV.Text = "IGV " + IGV.ToString("F") + "%:";

            fillCategory();

            fillGrid();

            try
            {
                int ID = (int)dataGrid.SelectedRows[0].Cells[0].Value;

                string msgError = "";
                List<Entity.Item> items = new Logic.Item().find(ref msgError, ID);
                if (items.Count > 0)
                {
                    suggestedPriceIsEdited = false;
                    EItem = items.First();
                    txtDescription.Text = EItem.Description;
                    txtCodigo.Text = EItem.Code;
                  
                    txtStock.Text = EItem.UnitsInStock.ToString();

                    var listItems = new List<listPrecio>();
                    listItems.Add(new listPrecio
                    {
                        ID = "unitPrice",
                        Description = "P. unitario: " + EItem.UnitPrice.ToString("F")
                    });
                    listItems.Add(new listPrecio
                    {
                        ID = "quarterPrice",
                        Description = "P. x /4: " + EItem.QuarterPrice.ToString("F")
                    });
                    listItems.Add(new listPrecio
                    {
                        ID = "dozenPrice",
                        Description = "P. x/12: " + EItem.DozenPrice.ToString("F")
                    });

                    cmbPrecio.DataSource = listItems;
                    cmbPrecio.ValueMember = "ID";
                    cmbPrecio.DisplayMember = "Description";
                    //cmbPrecio.SelectedIndex = 0;

                    calculate();
                }

            }
            catch (Exception)
            {
                return;
            }

           
        }

        private void fillCategory()
        {
            var msgError = "";
            var founds = new Logic.Category().find(ref msgError);
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var unspecified = new Entity.Category() { ID = 0, Description = "(No especificado)" };

            List<Entity.Category> reOrdered = new List<Entity.Category>();
            reOrdered.Add(unspecified);

            foreach (var item in founds)
            {
                reOrdered.Add(item);
            }

            cmbCategory.DataSource = reOrdered;
            cmbCategory.DisplayMember = "Description";
            cmbCategory.ValueMember = "ID";
        }

        private void fillGrid(string Description = null)
        {
        
            var msgError = "";
            int categoryID = Convert.ToInt32(cmbCategory.SelectedValue);
            List<Entity.Item> founds = new Logic.Item().find(ref msgError, 0, Description, categoryID);
            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var edited = founds.Where(x=>x.Discontinued == false).Select(x => new
            {
                ID = x.ID,
                x.Code,
                x.QuantityPerUnit,
                x.Description,
                x.DescriptionShort,
                ImageSmall = x.ImageSmall,
                x.CategoryDescription,
                x.UnitsInStock,
                x.UpdatedAt,
                x.CreatedAt
            }).ToList();

            if (rbOrderByStockDesc.Checked)
            {
                edited = edited.OrderByDescending(x => x.UnitsInStock).ToList();
            }
            else if (rbOrderByStockAsc.Checked)
            {
                edited = edited.OrderBy(x => x.UnitsInStock).ToList();
            }
            else if (rbOrderByDescription.Checked)
            {
                edited = edited.OrderBy(x => x.Description).ToList();
            }

            dataGrid.DataSource = edited;
        }

        private void txtFilterDescription_KeyUp(object sender, KeyEventArgs e)
        {
            string filter = txtFilterDescription.Text.Trim();
            fillGrid(filter);
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            EItem.AnnotationOnSale = txtAnnotation.Text;
            EItem.suggestedPriceIsEdited = suggestedPriceIsEdited;
            EItem.indexPrice = cmbPrecio.SelectedIndex;

            if (EItem.ID == 0)
            {
                MessageBox.Show("Debe seleccionar una opcion para agregar", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (EItem.QuantityOnSale == 0)
            {
                MessageBox.Show("Debe ingresar una cantidad mayor a 0", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (EItem.QuantityOnSale > EItem.UnitsInStock)
            {
                MessageBox.Show("La cantidad supera el stock del Producto.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var exists = Multimascot.sale.sale.items.FindIndex(x => x.ID == EItem.ID);

            if (exists >= 0)
            {
                var result = MessageBox.Show("¿El Producto ya existe en la lista desea reemplazarlo?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    Multimascot.sale.sale.items.RemoveAt(exists);
                    Multimascot.sale.sale.items.Add(EItem);
                }
                else
                {
                    return;
                }
            }
            else
            {
                Multimascot.sale.sale.items.Add(EItem);
            }

            btnClose.Text = "Terminar (y agregar " + Multimascot.sale.sale.items.Count.ToString() + ")";

            //this.DialogResult = DialogResult.OK;
        }

        string oldCmbPrecioValue = "";
        private void dataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int ID = 0;
            try
            {
                ID = (int)dataGrid.Rows[e.RowIndex].Cells[0].Value;
            }
            catch (Exception)
            {
                MessageBox.Show("La opcion seleccionada es incorrecta", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string msgError = "unitPrice";
            List<Entity.Item> items = new Logic.Item().find(ref msgError, ID);
            if (items.Count > 0)
            {
                suggestedPriceIsEdited = false;
                EItem = items.First();
                txtDescription.Text = EItem.Description;
                txtCodigo.Text = EItem.Code;
              
                txtStock.Text = EItem.UnitsInStock.ToString();
                txtAnnotation.Clear();

                var listItems = new List<listPrecio>();
                listItems.Add(new listPrecio
                {
                    ID = "unitPrice",
                    Description = "P. unitario: " + EItem.UnitPrice.ToString("F")
                });
                listItems.Add(new listPrecio
                {
                    ID = "quarterPrice",
                    Description = "P. x /4: " + EItem.QuarterPrice.ToString("F")
                });
                listItems.Add(new listPrecio
                {
                    ID = "dozenPrice",
                    Description = "P. x/12: " + EItem.DozenPrice.ToString("F")
                });

                oldCmbPrecioValue = cmbPrecio.SelectedValue.ToString();

                cmbPrecio.DataSource = listItems;
                cmbPrecio.ValueMember = "ID";
                cmbPrecio.DisplayMember = "Description";

                cmbPrecio.SelectedValue = oldCmbPrecioValue;

                calculate();
            }
        }

        private void txtQuantity_KeyUp(object sender, KeyEventArgs e)
        {
           calculate();
        }

        private void calculate()
        {
            btnSelect.Enabled = false;
            lblError.Text = "*";
            int quantity = 0;
            string _quantity = txtQuantity.Text.Trim();

            if (!Int32.TryParse(_quantity, out quantity))
            {
                lblError.Text = "El campo Cantidad debe ser un numero entero";
                txtQuantity.Text = "";
                return;
            }

            double suggestedPrice = 0;
            if (!suggestedPriceIsEdited)
            {
                if (cmbPrecio.SelectedValue.ToString() == "unitPrice")
                {
                    suggestedPrice = EItem.UnitPrice;
                }
                else if (cmbPrecio.SelectedValue.ToString() == "quarterPrice")
                {
                    suggestedPrice = EItem.QuarterPrice;

                }
                else if (cmbPrecio.SelectedValue.ToString() == "dozenPrice")
                {
                    suggestedPrice = EItem.DozenPrice;
                }
                txtSuggestedPrice.Text = suggestedPrice.ToString("F");
            }
            else
            {
                if (!Double.TryParse(txtSuggestedPrice.Text.Trim(), out suggestedPrice))
                {
                    lblError.Text = "El campo Precio sugerido debe ser un numero decimal";
                    txtSuggestedPrice.Text = "";
                    return;
                }
            }
            

            double subTotal = (quantity * suggestedPrice);
            //txtSubTotal.Text = subTotal.ToString("F");
            txtTotalNormal.Text = subTotal.ToString("F");

            double discount = 0;
            if (!Double.TryParse(txtDiscount.Text.Trim(), out discount))
            {
                lblError.Text = "El campo Descuento debe ser un numero decimal";
                txtDiscount.Text = "";
                return;
            }

            //double total = subTotal - discount;

            //double totalIGV = subTotal * IGV;
            //txtTotalIGV.Text = totalIGV.ToString("F");

            //txtTotal.Text = total.ToString("F");
            
            //double totalWithIGV = (subTotal + totalIGV);
            //txtTotalWithIgv.Text = totalWithIGV.ToString("F");


            //txtSubTotal.Text = (subTotal-totalIGV).ToString("F");
            //txtTotalWithIgv.Text = subTotal.ToString("F");

            //if (discount > 0)
            //{
                //double twigvDiscount = totalWithIGV - discount;
                //double _totalIGV = twigvDiscount * IGV;
                //double _totalItem = twigvDiscount - _totalIGV;

                //txtTotalIGV.Text = _totalIGV.ToString("F");
                //txtTotalWithIgv.Text = twigvDiscount.ToString("F");
                //txtSubTotal.Text = _totalItem.ToString("F");
            //}

            double total = subTotal - discount;

            double totalIGV = total * IGV;
            txtTotalIGV.Text = totalIGV.ToString("F");

            subTotal = total - totalIGV;

            txtTotalWithIgv.Text = total.ToString("F");
            txtSubTotal.Text = subTotal.ToString("F");

            EItem.QuantityOnSale        = quantity;
            EItem.SuggestedPriceOnSale  = suggestedPrice;
            EItem.DiscountOnSale        = discount;

            btnSelect.Enabled = true;
        }

        bool suggestedPriceIsEdited = false;
        private void txtSuggestedPrice_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtSuggestedPrice.Text.Trim().Length == 0)
            {
                suggestedPriceIsEdited = false;
            }
            else
            {
                double suggestedPrice = 0;
                if (!Double.TryParse(txtSuggestedPrice.Text.Trim(), out suggestedPrice))
                {
                    lblError.Text = "El campo Precio sugerido debe ser un numero decimal";
                    txtSuggestedPrice.Text = "";
                    suggestedPriceIsEdited = false;
                    return;
                }
                suggestedPriceIsEdited = true;
            }

           
            calculate();
        }

        private void cmbCategory_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string filter = txtFilterDescription.Text.Trim();
            fillGrid(filter);
        }

        private void rbOrderByStockDesc_CheckedChanged(object sender, EventArgs e)
        {
            if (!rbOrderByStockDesc.Checked)
            {
                fillGrid();
            }
        }

        private void rbOrderByStockAsc_CheckedChanged(object sender, EventArgs e)
        {
            if (!rbOrderByStockAsc.Checked)
            {
                fillGrid();
            }
        }

        private void rbOrderByDescription_CheckedChanged(object sender, EventArgs e)
        {
            if (!rbOrderByDescription.Checked)
            {
                fillGrid();
            }
        }

        private void cmbPrecio_SelectionChangeCommitted(object sender, EventArgs e)
        {
            calculate();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
