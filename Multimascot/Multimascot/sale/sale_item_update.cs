﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;

namespace Multimascot.sale
{
    public partial class sale_item_update : Form
    {
        Entity.Item item; 
        bool suggestedPriceIsEdited = false;
        public sale_item_update(Entity.Item _item)
        {
            InitializeComponent();
            item = _item;
        }
        private class listPrecio
        {
            public string ID { get; set; }
            public string Description { get; set; }
        }
        double IGV = 0;
        private void sale_item_update_Load(object sender, EventArgs e)
        {
            //get Tax:last
            string msgErrorx = "";
            var taxes = new Logic.Tax().find(ref msgErrorx);
            if (taxes.Count > 0)
            {
                IGV = taxes[0].Percentage; //assuming that this in descending order by creation date
            }

            lblIGV.Text = "IGV " + IGV.ToString("F") + "%:";


            txtDescription.Text = item.Description;
            txtCodigo.Text          = item.Code;
          
            txtAnnotation.Text      = item.AnnotationOnSale;
            txtDiscount.Text        = item.DiscountOnSale.ToString("F");
            txtQuantity.Text        = item.QuantityOnSale.ToString();
            txtSuggestedPrice.Text  = item.SuggestedPriceOnSale.ToString("F");
            suggestedPriceIsEdited  = item.suggestedPriceIsEdited;
            txtStock.Text           = item.UnitsInStock.ToString();

            var listItems = new List<listPrecio>();
            listItems.Add(new listPrecio
            {
                ID = "unitPrice",
                Description = "P. unitario: " + item.UnitPrice.ToString("F")
            });
            listItems.Add(new listPrecio
            {
                ID = "quarterPrice",
                Description = "P. x /4: " + item.QuarterPrice.ToString("F")
            });
            listItems.Add(new listPrecio
            {
                ID = "dozenPrice",
                Description = "P. x/12: " + item.DozenPrice.ToString("F")
            });

            cmbPrecio.DataSource = listItems;
            cmbPrecio.ValueMember = "ID";
            cmbPrecio.DisplayMember = "Description";
            cmbPrecio.SelectedIndex = item.indexPrice;

            calculate();
        }


        private void calculate()
        {
            btnSelect.Enabled = false;
            lblError.Text = "*";
            int quantity = 0;
            string _quantity = txtQuantity.Text.Trim();

            if (!Int32.TryParse(_quantity, out quantity))
            {
                lblError.Text = "El campo Cantidad debe ser un numero entero";
                txtQuantity.Text = "";
                return;
            }

            double suggestedPrice = 0;
            if (!suggestedPriceIsEdited)
            {
                if (cmbPrecio.SelectedValue.ToString() == "unitPrice")
                {
                    suggestedPrice = item.UnitPrice;
                }
                else if (cmbPrecio.SelectedValue.ToString() == "quarterPrice")
                {
                    suggestedPrice = item.QuarterPrice;

                }
                else if (cmbPrecio.SelectedValue.ToString() == "dozenPrice")
                {
                    suggestedPrice = item.DozenPrice;
                }
                txtSuggestedPrice.Text = suggestedPrice.ToString("F");
            }
            else
            {
                if (!Double.TryParse(txtSuggestedPrice.Text.Trim(), out suggestedPrice))
                {
                    lblError.Text = "El campo Precio sugerido debe ser un numero decimal";
                    txtSuggestedPrice.Text = "";
                    return;
                }
            }

            double subTotal = (quantity * suggestedPrice);
            txtSubTotal.Text = subTotal.ToString("F");
            txtTotalNormal.Text = subTotal.ToString("F");

            double discount = 0;
            if (!Double.TryParse(txtDiscount.Text.Trim(), out discount))
            {
                lblError.Text = "El campo Descuento debe ser un numero decimal";
                txtDiscount.Text = "";
                return;
            }

            //double totalIGV = subTotal * IGV;
            //txtTotalIGV.Text = totalIGV.ToString("F");

            ////txtTotal.Text = total.ToString("F");

            //double totalWithIGV = (subTotal + totalIGV);
            //txtTotalWithIgv.Text = totalWithIGV.ToString("F");


            //if (discount > 0)
            //{
            //    double twigvDiscount = totalWithIGV - discount;
            //    double _totalIGV = twigvDiscount * IGV;
            //    double _totalItem = twigvDiscount - _totalIGV;

            //    txtTotalIGV.Text = _totalIGV.ToString("F");
            //    txtTotalWithIgv.Text = twigvDiscount.ToString("F");
            //    txtSubTotal.Text = _totalItem.ToString("F");
            //}


            double total = subTotal - discount;

            double totalIGV = total * IGV;
            txtTotalIGV.Text = totalIGV.ToString("F");

            subTotal = total - totalIGV;

            txtTotalWithIgv.Text = total.ToString("F");
            txtSubTotal.Text = subTotal.ToString("F");

            item.QuantityOnSale         = quantity;
            item.SuggestedPriceOnSale   = suggestedPrice;
            item.DiscountOnSale         = discount;

            btnSelect.Enabled = true;
        }

        private void txtQuantity_KeyUp(object sender, KeyEventArgs e)
        {
            calculate();
        }

        private void txtSuggestedPrice_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtSuggestedPrice.Text.Trim().Length == 0)
            {
                suggestedPriceIsEdited = false;
            }
            else
            {
                double suggestedPrice = 0;
                if (!Double.TryParse(txtSuggestedPrice.Text.Trim(), out suggestedPrice))
                {
                    lblError.Text = "El campo Precio sugerido debe ser un numero decimal";
                    txtSuggestedPrice.Text = "";
                    suggestedPriceIsEdited = false;
                    return;
                }
                suggestedPriceIsEdited = true;
            }


            calculate();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            item.AnnotationOnSale = txtAnnotation.Text;
            item.suggestedPriceIsEdited = suggestedPriceIsEdited;

            if (item.QuantityOnSale == 0)
            {
                MessageBox.Show("Debe ingresar una cantidad mayor a 0", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (item.QuantityOnSale > item.UnitsInStock)
            {
                MessageBox.Show("La cantidad supera el stock del Producto.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var exists = Multimascot.sale.sale.items.FindIndex(x => x.ID == item.ID);

            if (exists >= 0)
            {
               
                    Multimascot.sale.sale.items.RemoveAt(exists);
                    Multimascot.sale.sale.items.Add(item);
                    this.DialogResult = DialogResult.OK;
            }

        }

        private void cmbPrecio_SelectionChangeCommitted(object sender, EventArgs e)
        {
            calculate();
        }
    }
}
