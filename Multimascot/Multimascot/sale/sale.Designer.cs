﻿namespace Multimascot.sale
{
    partial class sale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtClientDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetClient = new System.Windows.Forms.Button();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtAnnotation = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGrid = new System.Windows.Forms.DataGridView();
            this.gID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.image = new System.Windows.Forms.DataGridViewImageColumn();
            this.QuantityPerUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gSuggestedPriceOnSale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gQuantityOnSale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gTotalSimple = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gDiscountOnSale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gSubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gIGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gAnnotationOnSale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDeleteItem = new System.Windows.Forms.Button();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTax = new System.Windows.Forms.TextBox();
            this.lblIGV = new System.Windows.Forms.Label();
            this.txtReceipt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtChange = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPreCant = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SuggestedPriceOnSale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalSimple = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuantityOnSale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(650, 403);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(148, 49);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "Registrar";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtClientDescription
            // 
            this.txtClientDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClientDescription.Location = new System.Drawing.Point(75, 17);
            this.txtClientDescription.MaxLength = 0;
            this.txtClientDescription.Name = "txtClientDescription";
            this.txtClientDescription.ReadOnly = true;
            this.txtClientDescription.Size = new System.Drawing.Size(327, 22);
            this.txtClientDescription.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cliente:*";
            // 
            // btnGetClient
            // 
            this.btnGetClient.Location = new System.Drawing.Point(408, 16);
            this.btnGetClient.Name = "btnGetClient";
            this.btnGetClient.Size = new System.Drawing.Size(104, 23);
            this.btnGetClient.TabIndex = 6;
            this.btnGetClient.Text = "Buscar o Agregar";
            this.btnGetClient.UseVisualStyleBackColor = true;
            this.btnGetClient.Click += new System.EventHandler(this.btnGetClient_Click);
            // 
            // txtDate
            // 
            this.txtDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(703, 18);
            this.txtDate.MaxLength = 100;
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(95, 22);
            this.txtDate.TabIndex = 7;
            this.txtDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAnnotation
            // 
            this.txtAnnotation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAnnotation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnotation.Location = new System.Drawing.Point(15, 379);
            this.txtAnnotation.MaxLength = 0;
            this.txtAnnotation.Multiline = true;
            this.txtAnnotation.Name = "txtAnnotation";
            this.txtAnnotation.Size = new System.Drawing.Size(198, 70);
            this.txtAnnotation.TabIndex = 42;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 355);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 41;
            this.label5.Text = "Observacion:";
            // 
            // dataGrid
            // 
            this.dataGrid.AllowUserToAddRows = false;
            this.dataGrid.AllowUserToDeleteRows = false;
            this.dataGrid.AllowUserToOrderColumns = true;
            this.dataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGrid.ColumnHeadersHeight = 25;
            this.dataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gID,
            this.image,
            this.QuantityPerUnit,
            this.gDescription,
            this.gSuggestedPriceOnSale,
            this.gQuantityOnSale,
            this.gTotalSimple,
            this.gDiscountOnSale,
            this.gSubTotal,
            this.gIGV,
            this.gTotal,
            this.gAnnotationOnSale});
            this.dataGrid.Location = new System.Drawing.Point(8, 34);
            this.dataGrid.MultiSelect = false;
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.ReadOnly = true;
            this.dataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGrid.Size = new System.Drawing.Size(769, 262);
            this.dataGrid.TabIndex = 43;
            this.dataGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_CellDoubleClick);
            // 
            // gID
            // 
            this.gID.DataPropertyName = "ID";
            this.gID.HeaderText = "ID";
            this.gID.Name = "gID";
            this.gID.ReadOnly = true;
            this.gID.Width = 43;
            // 
            // image
            // 
            this.image.DataPropertyName = "image";
            this.image.HeaderText = "Imagen";
            this.image.Name = "image";
            this.image.ReadOnly = true;
            this.image.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.image.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.image.Width = 67;
            // 
            // QuantityPerUnit
            // 
            this.QuantityPerUnit.DataPropertyName = "QuantityPerUnit";
            this.QuantityPerUnit.HeaderText = "Talla";
            this.QuantityPerUnit.Name = "QuantityPerUnit";
            this.QuantityPerUnit.ReadOnly = true;
            this.QuantityPerUnit.Width = 55;
            // 
            // gDescription
            // 
            this.gDescription.DataPropertyName = "Description";
            this.gDescription.HeaderText = "Producto";
            this.gDescription.Name = "gDescription";
            this.gDescription.ReadOnly = true;
            this.gDescription.Width = 75;
            // 
            // gSuggestedPriceOnSale
            // 
            this.gSuggestedPriceOnSale.DataPropertyName = "SuggestedPriceOnSale";
            this.gSuggestedPriceOnSale.HeaderText = "Precio de venta";
            this.gSuggestedPriceOnSale.Name = "gSuggestedPriceOnSale";
            this.gSuggestedPriceOnSale.ReadOnly = true;
            this.gSuggestedPriceOnSale.Width = 107;
            // 
            // gQuantityOnSale
            // 
            this.gQuantityOnSale.DataPropertyName = "QuantityOnSale";
            this.gQuantityOnSale.HeaderText = "Cantidad";
            this.gQuantityOnSale.Name = "gQuantityOnSale";
            this.gQuantityOnSale.ReadOnly = true;
            this.gQuantityOnSale.Width = 74;
            // 
            // gTotalSimple
            // 
            this.gTotalSimple.DataPropertyName = "TotalSimple";
            this.gTotalSimple.HeaderText = "Pre. x Cant.";
            this.gTotalSimple.Name = "gTotalSimple";
            this.gTotalSimple.ReadOnly = true;
            this.gTotalSimple.Width = 87;
            // 
            // gDiscountOnSale
            // 
            this.gDiscountOnSale.DataPropertyName = "DiscountOnSale";
            this.gDiscountOnSale.HeaderText = "Descuento";
            this.gDiscountOnSale.Name = "gDiscountOnSale";
            this.gDiscountOnSale.ReadOnly = true;
            this.gDiscountOnSale.Width = 84;
            // 
            // gSubTotal
            // 
            this.gSubTotal.DataPropertyName = "SubTotal";
            this.gSubTotal.HeaderText = "SubTotal";
            this.gSubTotal.Name = "gSubTotal";
            this.gSubTotal.ReadOnly = true;
            this.gSubTotal.Width = 75;
            // 
            // gIGV
            // 
            this.gIGV.DataPropertyName = "IGV";
            this.gIGV.HeaderText = "IGV";
            this.gIGV.Name = "gIGV";
            this.gIGV.ReadOnly = true;
            this.gIGV.Width = 50;
            // 
            // gTotal
            // 
            this.gTotal.DataPropertyName = "Total";
            this.gTotal.HeaderText = "Total";
            this.gTotal.Name = "gTotal";
            this.gTotal.ReadOnly = true;
            this.gTotal.Width = 56;
            // 
            // gAnnotationOnSale
            // 
            this.gAnnotationOnSale.DataPropertyName = "AnnotationOnSale";
            this.gAnnotationOnSale.HeaderText = "Observacion";
            this.gAnnotationOnSale.Name = "gAnnotationOnSale";
            this.gAnnotationOnSale.ReadOnly = true;
            this.gAnnotationOnSale.Width = 92;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.dataGrid);
            this.groupBox1.Location = new System.Drawing.Point(15, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(783, 302);
            this.groupBox1.TabIndex = 44;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalle";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnDeleteItem);
            this.panel1.Controls.Add(this.btnAddItem);
            this.panel1.Location = new System.Drawing.Point(622, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(155, 25);
            this.panel1.TabIndex = 44;
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.Location = new System.Drawing.Point(77, 0);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteItem.TabIndex = 45;
            this.btnDeleteItem.Text = "Remover";
            this.btnDeleteItem.UseVisualStyleBackColor = true;
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.Location = new System.Drawing.Point(1, 0);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(75, 23);
            this.btnAddItem.TabIndex = 44;
            this.btnAddItem.Text = "Agregar";
            this.btnAddItem.UseVisualStyleBackColor = true;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(429, 355);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 16);
            this.label1.TabIndex = 45;
            this.label1.Text = "Sub. Total S/.:";
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubTotal.Location = new System.Drawing.Point(526, 354);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.ReadOnly = true;
            this.txtSubTotal.Size = new System.Drawing.Size(86, 20);
            this.txtSubTotal.TabIndex = 46;
            this.txtSubTotal.Text = "0.00";
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotal
            // 
            this.txtTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(695, 377);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(100, 20);
            this.txtTotal.TabIndex = 50;
            this.txtTotal.Text = "0.00";
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(634, 380);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 49;
            this.label4.Text = "Total S/.:";
            // 
            // txtTax
            // 
            this.txtTax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTax.Location = new System.Drawing.Point(695, 355);
            this.txtTax.Name = "txtTax";
            this.txtTax.ReadOnly = true;
            this.txtTax.Size = new System.Drawing.Size(100, 20);
            this.txtTax.TabIndex = 52;
            this.txtTax.Text = "0.00";
            this.txtTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblIGV
            // 
            this.lblIGV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIGV.AutoSize = true;
            this.lblIGV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIGV.Location = new System.Drawing.Point(629, 358);
            this.lblIGV.Name = "lblIGV";
            this.lblIGV.Size = new System.Drawing.Size(55, 16);
            this.lblIGV.TabIndex = 51;
            this.lblIGV.Text = "IGV 0%:";
            // 
            // txtReceipt
            // 
            this.txtReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceipt.Location = new System.Drawing.Point(494, 409);
            this.txtReceipt.Name = "txtReceipt";
            this.txtReceipt.Size = new System.Drawing.Size(100, 20);
            this.txtReceipt.TabIndex = 56;
            this.txtReceipt.Text = "0.00";
            this.txtReceipt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtReceipt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtReceipt_KeyUp);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(422, 410);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 16);
            this.label7.TabIndex = 55;
            this.label7.Text = "Recibo S/.:";
            // 
            // txtChange
            // 
            this.txtChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChange.Location = new System.Drawing.Point(494, 433);
            this.txtChange.Name = "txtChange";
            this.txtChange.ReadOnly = true;
            this.txtChange.Size = new System.Drawing.Size(100, 20);
            this.txtChange.TabIndex = 58;
            this.txtChange.Text = "0.00";
            this.txtChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(429, 434);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 16);
            this.label8.TabIndex = 57;
            this.label8.Text = "Vuelto S/.:";
            // 
            // txtPreCant
            // 
            this.txtPreCant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPreCant.Location = new System.Drawing.Point(313, 354);
            this.txtPreCant.Name = "txtPreCant";
            this.txtPreCant.ReadOnly = true;
            this.txtPreCant.Size = new System.Drawing.Size(100, 20);
            this.txtPreCant.TabIndex = 60;
            this.txtPreCant.Text = "0.00";
            this.txtPreCant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(218, 355);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 16);
            this.label6.TabIndex = 59;
            this.label6.Text = "Pre. x Cant. S/.:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(219, 382);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 16);
            this.label3.TabIndex = 47;
            this.label3.Text = "Descuento S/.:";
            // 
            // txtDiscount
            // 
            this.txtDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiscount.Location = new System.Drawing.Point(313, 381);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.ReadOnly = true;
            this.txtDiscount.Size = new System.Drawing.Size(100, 20);
            this.txtDiscount.TabIndex = 48;
            this.txtDiscount.Text = "0.00";
            this.txtDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            this.ID.Width = 43;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "Descripcion";
            this.Description.Name = "Description";
            this.Description.Width = 88;
            // 
            // SuggestedPriceOnSale
            // 
            this.SuggestedPriceOnSale.DataPropertyName = "SuggestedPriceOnSale";
            this.SuggestedPriceOnSale.HeaderText = "Precio unitario";
            this.SuggestedPriceOnSale.Name = "SuggestedPriceOnSale";
            this.SuggestedPriceOnSale.Width = 99;
            // 
            // TotalSimple
            // 
            this.TotalSimple.HeaderText = "Pre. x Cant.";
            this.TotalSimple.Name = "TotalSimple";
            // 
            // QuantityOnSale
            // 
            this.QuantityOnSale.DataPropertyName = "QuantityOnSale";
            this.QuantityOnSale.HeaderText = "Cantidad";
            this.QuantityOnSale.Name = "QuantityOnSale";
            this.QuantityOnSale.Width = 74;
            // 
            // sale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 462);
            this.Controls.Add(this.txtPreCant);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtChange);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtReceipt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtTax);
            this.Controls.Add(this.lblIGV);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDiscount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSubTotal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtAnnotation);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.btnGetClient);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtClientDescription);
            this.Controls.Add(this.label2);
            this.Name = "sale";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Venta";
            this.Load += new System.EventHandler(this.sale_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtClientDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGetClient;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtAnnotation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGrid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDeleteItem;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSubTotal;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTax;
        private System.Windows.Forms.Label lblIGV;
        private System.Windows.Forms.TextBox txtReceipt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtChange;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPreCant;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDiscount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn SuggestedPriceOnSale;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalSimple;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuantityOnSale;
        private System.Windows.Forms.DataGridViewTextBoxColumn gID;
        private System.Windows.Forms.DataGridViewImageColumn image;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuantityPerUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn gDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn gSuggestedPriceOnSale;
        private System.Windows.Forms.DataGridViewTextBoxColumn gQuantityOnSale;
        private System.Windows.Forms.DataGridViewTextBoxColumn gTotalSimple;
        private System.Windows.Forms.DataGridViewTextBoxColumn gDiscountOnSale;
        private System.Windows.Forms.DataGridViewTextBoxColumn gSubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn gIGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn gTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn gAnnotationOnSale;
    }
}