﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entity;
using Logic;
namespace Multimascot.sale
{
    public partial class sale : Form
    {
        public sale()
        {
            InitializeComponent();
        }

        public static Entity.Customer customer = new Entity.Customer();

        double IGV = 0;

        public static Entity.Sale ESale = new Entity.Sale();
        private void btnGetClient_Click(object sender, EventArgs e)
        {
            Multimascot.sale.customer form = new customer(this);
            var result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                string client = "";
                if (customer.RUC.Length > 0)
                {
                    client = "DNI/RUC: " + customer.RUC + " | ";
                }
                client += customer.Name;

                txtClientDescription.Text = client;
            }
        }

        public static List<Entity.Item> items = new List<Entity.Item>();
        private void sale_Load(object sender, EventArgs e)
        {
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            //get Tax:last
            string msgError = "";
            var taxes = new Logic.Tax().find(ref msgError);
            if (taxes.Count > 0)
            {
                IGV = taxes[0].Percentage; //assuming that this in descending order by creation date
                ESale.taxID = taxes[0].ID;
            }

            lblIGV.Text = "IGV " + IGV.ToString("F") + "%";

            this.WindowState = FormWindowState.Maximized;
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            Multimascot.sale.item form = new item();
            var result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                fillGrid();
            }
        }


        private class gridData
        {
            public int ID { get; set; }
            public object image { get; set; }
            public string Description { get; set; }
            public string QuantityPerUnit { get; set; }
            public double SuggestedPriceOnSale { get; set; }
            public double QuantityOnSale { get; set; }
            public double TotalSimple { get; set; }
            public double SubTotal { get; set; }
            public double IGV { get; set; }
            public double Total { get; set; }
            public double DiscountOnSale { get; set; }
            public string AnnotationOnSale { get; set; }
        }

        double subTotal = 0, discount = 0, total = 0, totalTax = 0, totalWithTax = 0, totalIGV = 0, preXcant = 0;
        private void fillGrid()
        {
            subTotal = discount = total = totalTax = totalWithTax = totalIGV = preXcant = 0;
            List<gridData> newList = items.Select(x => new gridData()
            {
                ID = x.ID,
                image = x.ImageSmall,
                Description = x.Description,
                QuantityPerUnit = x.QuantityPerUnit,
                SuggestedPriceOnSale= x.SuggestedPriceOnSale,
                QuantityOnSale = x.QuantityOnSale,
                TotalSimple = x.SuggestedPriceOnSale * x.QuantityOnSale,
                SubTotal = ((x.SuggestedPriceOnSale * x.QuantityOnSale) - x.DiscountOnSale) - (((x.SuggestedPriceOnSale * x.QuantityOnSale) - x.DiscountOnSale) * IGV),
                IGV = ((x.SuggestedPriceOnSale * x.QuantityOnSale) - x.DiscountOnSale) * IGV,
                Total = (x.SuggestedPriceOnSale * x.QuantityOnSale) - x.DiscountOnSale,
                DiscountOnSale = x.DiscountOnSale,
                AnnotationOnSale = x.AnnotationOnSale
            }).ToList();

            foreach (var item in newList)
            {
                /*
                if (item.DiscountOnSale > 0)
                {
                    double twigvDiscount = item.Total - item.DiscountOnSale;
                    double _totalIGV = twigvDiscount * IGV;
                    double _totalItem = twigvDiscount - _totalIGV;

                    item.SubTotal = _totalItem;
                    item.IGV = _totalIGV;
                    item.Total = twigvDiscount;
                }*/

                subTotal    += item.SubTotal;
                discount    += item.DiscountOnSale;
                total       += item.Total;
                totalIGV    += item.IGV;
                preXcant += item.QuantityOnSale * item.SuggestedPriceOnSale;

                /*ROUND*/
                item.SubTotal = Math.Round(item.SubTotal,2);
                item.DiscountOnSale = Math.Round(item.DiscountOnSale,2);
                item.Total = Math.Round(item.Total,2);
                item.IGV = Math.Round(item.IGV,2);
            }

            totalTax        = totalIGV;
            totalWithTax    = total;

            txtSubTotal.Text    = subTotal.ToString("F");
            txtDiscount.Text    = discount.ToString("F");
            txtTax.Text         = totalTax.ToString("F");
            txtTotal.Text       = totalWithTax.ToString("F");
            txtPreCant.Text     = preXcant.ToString("F");

            calculateChange();
            dataGrid.DataSource = newList;
        }

        private void dataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int ID = 0;
            try
            {
                ID = (int)dataGrid.Rows[e.RowIndex].Cells[0].Value;
            }
            catch (Exception) { return; }

            if (ID > 0)
            {
                Entity.Item item = items.Find(x => x.ID == ID);
                Multimascot.sale.sale_item_update form = new sale_item_update(item);

                var response = form.ShowDialog();
                if (response == DialogResult.OK)
                {
                    fillGrid();
                }
            }
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            int ID = 0;
            try
            {
                ID = (int)dataGrid.SelectedRows[0].Cells[0].Value;
            }
            catch (Exception)
            {
                MessageBox.Show("La opcion seleccionada es incorrecta", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var exists = Multimascot.sale.sale.items.FindIndex(x => x.ID == ID);
            if (exists >= 0)
            {
                Multimascot.sale.sale.items.RemoveAt(exists);
                fillGrid();
            }
        }

        private void txtReceipt_KeyUp(object sender, KeyEventArgs e)
        {
            calculateChange();
        }

        private void calculateChange()
        {
            try
            {
                double receipt = 0;
                receipt = Convert.ToDouble(txtReceipt.Text);
                txtChange.Text = (receipt - totalWithTax).ToString("F");
            }
            catch (Exception)
            {
                txtChange.Clear();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ESale.Annotation = txtAnnotation.Text;

            if (MessageBox.Show("¿Esta seguro/a de registrar este Cliente?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            if (customer.ID == 0)
            {
                MessageBox.Show("Debe agregar un cliente para continuar", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            ESale.CustomerID = customer.ID;
            ESale.UserID = user.login.logged.ID;

            if (items.Count == 0)
            {
                MessageBox.Show("Debe agregar un producto para continuar", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string msgError = "";
            int saleID = new Logic.Sale().create(ref msgError, ESale, items);

            if (!String.IsNullOrEmpty(msgError))
            {
                MessageBox.Show(msgError, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var result =  MessageBox.Show("La Venta ha sido registrada con exito. " + Environment.NewLine + " ¿Desea imprimir la boleta/factura?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                report.saleDetail form = new report.saleDetail(saleID);
                form.ShowDialog();
            }

            customer = new Entity.Customer();
            txtAnnotation.Clear();
            items = new List<Entity.Item>();
            txtReceipt.Text = "0.00";
            txtChange.Text  = "0.00";
            txtClientDescription.Clear();

            fillGrid();

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

    }
}
