﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class Item
    {
        public int      ID              { get; set; }
        public string   Code            { get; set; }
        public double   Cost            { get; set; }
        public string   DescriptionShort { get; set; }
        public string   Description     { get; set; }
        public object   ImageSmall      { get; set; }
        public object   ImageBig        { get; set; }
        public int      SupplierID      { get; set; }
        public int      CategoryID      { get; set; }
        public double   UnitPrice       { get; set; }
        public double   QuarterPrice    { get; set; }
        public double   DozenPrice      { get; set; }
        public string   QuantityPerUnit { get; set; }
        public int      UnitsInStock    { get; set; }
        public bool     Discontinued    { get; set; }
        public string   UpdatedAt       { get; set; }
        public string   CreatedAt       { get; set; }

        public string   SupplierDescription { get; set; }
        public string   CategoryDescription { get; set; }

        public double   SuggestedPriceOnSale    { get; set; }
        public double   QuantityOnSale          { get; set; }
        public double   DiscountOnSale          { get; set; }
        public string   AnnotationOnSale        { get; set; }
        public bool     suggestedPriceIsEdited  { get; set; }
        public int      indexPrice              { get; set; }

    }
}
