﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class Customer
    {
        public int      ID          { get; set; }
        public string   RUC         { get; set; }
        public string   Name        { get; set; }
        public string   Phone       { get; set; }
        public string   Annotation  { get; set; }
        public string   UpdatedAt   { get; set; }
        public string   CreatedAt   { get; set; }
        public string   Email       { get; set; }
        public string   Address     { get; set; }
    }
}
