﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class Supplier
    {
        public int      ID          { get; set; }
        public string   Description { get; set; }
        public string   RUC         { get; set; }
        public string   Phone       { get; set; }
        public string   Annotation  { get; set; }
        public string   UpdatedAt   { get; set; }
        public string   CreatedAt   { get; set; }
        public string   Address     { get; set; }
    }
}
