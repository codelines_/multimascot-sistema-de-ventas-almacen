﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class Category
    {
        public int      ID          { get; set; }
        public string   Description { get; set; }
        public string   UpdatedAt   { get; set; }
        public string   CreatedAt   { get; set; }
    }
}
