﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class SaleDetail
    {
        public int      SaleID                  { get; set; }
        public int      ItemID                  { get; set; }
        public double   Cost                    { get; set; }
        public double   UnitPriceOnSale         { get; set; }
        public double   QuarterPriceOnSale      { get; set; }
        public double   DozenPriceOnSale        { get; set; }
        public double   QuantityPerUnitOnSale   { get; set; }
        public double   RealUnitPrice           { get; set; }
        public int      Quantity                { get; set; }
        public double   Discount                { get; set; }
        public string   Annotation              { get; set; }
    }
}
