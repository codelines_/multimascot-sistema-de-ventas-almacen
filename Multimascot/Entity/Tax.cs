﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class Tax
    {
        public int      ID          { get; set; }
        public double   Percentage  { get; set; }
        public string   UpdatedAt   { get; set; }
        public string   CreatedAt   { get; set; }
    }
}
