﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class Sale
    {
        public int      ID          { get; set; }
        public int      CustomerID  { get; set; }
        public int      UserID      { get; set; }
        public string   Date        { get; set; }
        public string   Status      { get; set; }
        public string   Annotation  { get; set; }
        public string   UpdatedAt   { get; set; }
        public string   CreatedAt   { get; set; }
        public int      taxID       { get; set; }
    }
}
