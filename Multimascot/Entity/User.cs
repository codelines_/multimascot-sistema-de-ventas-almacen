﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class User
    {
        public int      ID              { get; set; }
        public string   Name            { get; set; }
        public string   IdentityCard    { get; set; }
        public string   Phone           { get; set; }
        public string   Account         { get; set; }
        public string   Password        { get; set; }
        public bool     Status          { get; set; }
        public bool     Admin           { get; set; }
        public string   UpdatedAt       { get; set; }
        public string   CreatedAt       { get; set; }
    }
}
